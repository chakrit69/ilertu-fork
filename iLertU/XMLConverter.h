//
//  XMLConverter.h
//  XMLConverter
//
//  Created by Katanyoo Ubalee on 4/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMLConverter : NSObject

- (id)parseWithString:(NSString *)xmlString;

@end
