//
//  MyServiceViewController.m
//  iLertU
//
//  Created by Chakrit Paniam on 3/11/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "MyServiceViewController.h"

#import "XMLConverter.h"

#import "AddServiceController.h"
#import "WebViewController.h"
#import "LertViewController.h"
#import "ASIFormDataRequest.h"

#import "UserEntity.h"




@implementation MyServiceViewController


@synthesize myServiceArray;
@synthesize user;
@synthesize service;


@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;


-(void)insertDataFormXML:(id)xmlResult{
    
    self.myServiceArray = [[NSMutableArray alloc] init];
    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        NSLog(@"xmlResult = %@",xmlResult);
        
        for (NSDictionary *i in [xmlResult objectForKey:@"services"]) {
            if ([[i objectForKey:@"active"] isEqualToString:@"true"]) {  
                [self.myServiceArray addObject:i];
            }
        }
        
        
        //self.myServiceArray = [xmlResult objectForKey:@"services"];
    }
    [self.tableView reloadData];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithUser:(UserEntity *)_user{
    self = [super initWithNibName:@"MyServiceViewController" bundle:nil];
    if (self) {
        self.user = _user;
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)addCattagory:(id)sender{
    AddServiceController *addSeiver = [[AddServiceController alloc] initWithUser:self.user];
    [self.navigationController pushViewController:addSeiver animated:YES];
    //[addSeiver release];
}
-(void)setupUI{
    self.title = @"i services";
    
    UIBarButtonItem *addBotton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addCattagory:)];
    self.navigationItem.rightBarButtonItem = addBotton;
}
-(void)setupData{
    
    
    NSString *pathUrl = [NSString stringWithFormat:@"%@/user/services/list",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?><lertParams type='ilertu.user.services-list'>                        <common>                        <device>                        <deviceType>ios</deviceType>    <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate></lastedUpdateDate>                        <language>%@</language>                        </common>                        </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,Language];
    
    
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
        NSLog(@"%@",responseString);
        XMLConverter *xml = [[XMLConverter alloc] init];
        [self insertDataFormXML:[xml parseWithString:responseString]];
        
        //[self.tableView reloadData];
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    
    
    
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUI];
    [self setupData];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.myServiceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    cell.accessoryType = 1;
    
    cell.textLabel.text = [[self.myServiceArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.detailTextLabel.text = [[self.myServiceArray objectAtIndex:indexPath.row] objectForKey:@"desc"];
    
    NSURL* aURL = [NSURL URLWithString:[[self.myServiceArray objectAtIndex:indexPath.row] objectForKey:@"imageUrl"]];
    NSData* data = [[NSData alloc] initWithContentsOfURL:aURL];
    
    //  NSString * imagePath = [NSURL URLWithString:[[self.myServiceArray objectAtIndex:indexPath.row] objectForKey:@"imageUrl"]];
    cell.imageView.image = [UIImage imageWithData:data];
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Action for Service" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Lert", @"View Service Information",@"Delete", nil];
    selectIndex = indexPath.row;
	popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
	[popupQuery showInView:self.view];
	[popupQuery release];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		LertViewController *lertView = [[LertViewController alloc] initWithNibName:@"LertViewController" bundle:nil];
        [self.navigationController pushViewController:lertView animated:YES];
	
    } else if (buttonIndex == 1) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
        NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
        
        NSString *Language;
        Language = [plistDict objectForKey:@"Language"];
        
        NSString *string = [NSString stringWithFormat:@"http://202.60.194.200/ilertu_addins/ClientWeb/0_1/Service/View/%@?serviceOfUserId=%@&lang=%@&deviceType=ios&deviceId=%@&currentVersion=1.0a",self.user.idUser,[[self.myServiceArray objectAtIndex:selectIndex] objectForKey:@"id"],Language,[[UIDevice currentDevice] uniqueIdentifier]];
        NSLog(@"string %@",string);
        WebViewController *webView = [[WebViewController alloc] initWithURL:string];
        [self.navigationController pushViewController:webView animated:YES];
	
    } else if (buttonIndex == 2) {
        
        UIAlertView *alertDelete = [[UIAlertView alloc] initWithTitle:@"Confirmation" message:@"Do you want to Delete 'Service'"  delegate:self cancelButtonTitle:@"Cancle" otherButtonTitles: @"Delete", nil];
		[alertDelete show];
		[alertDelete release];
        

	} else if (buttonIndex == 3) {
		NSLog(@"3");
	}
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // the user clicked one of the OK/Cancel buttons
    if (buttonIndex == 0)
    {
        NSLog(@"cancel");
    }
    else
    {
        NSLog(@"ok");
        
        NSString *pathUrl = [NSString stringWithFormat:@"%@/user/services/delete",ILERTU];
        NSURL *url = [NSURL URLWithString:pathUrl];
        
        
        ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
        
        [request2 addRequestHeader:@"content-type" value:@"text/xml"];
        
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
        NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
        
        NSString *Language;
        Language = [plistDict objectForKey:@"Language"];
        
        NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?><lertParams type='ilertu.user.services-delete'>                        <common>                        <device>                        <deviceType>ios</deviceType>    <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>0</lastedUpdateDate>                        <language>%@</language>                        </common> <service id='%@'/>                       </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,Language,[[self.myServiceArray objectAtIndex:selectIndex] objectForKey:@"id"]];
        
        NSLog(@"delete %@",string);
        
        [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [request2 setCompletionBlock:^{
            NSString *responseString = [request2 responseString];
            
           // XMLConverter *xml = [[XMLConverter alloc] init];
           // [self insertDataFormXML:[xml parseWithString:responseString]];
         [self setupData];
            
            
        }];
        
        [request2 setFailedBlock:^{
            NSError *error = [request2 error];
            NSLog(@"Error: %@", error.localizedDescription);
            
            UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertMessageView show];
            [alertMessageView release];
            
        }];
        
        [request2 startAsynchronous];
        
        


    }
}

@end
