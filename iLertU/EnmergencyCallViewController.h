//
//  EnmergencyCallViewController.h
//  iLertU
//
//  Created by Chakrit Paniam on 3/9/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnmergencyCallViewController : UITableViewController <UIActionSheetDelegate>{
    
}
@property(nonatomic, retain)NSArray* callNumber;
@property(nonatomic, retain)NSArray* callDest;
@property(nonatomic, retain)NSArray* callImage;

@property(nonatomic, retain)NSString* telNumber;

@end
