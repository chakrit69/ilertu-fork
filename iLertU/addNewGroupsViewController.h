//
//  addNewGroupsViewController.h
//  iLertU
//
//  Created by qoo hudsioo on 4/12/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UserEntity;
@interface addNewGroupsViewController : UITableViewController<UITextFieldDelegate>{
    

    
}

@property (nonatomic , retain) UserEntity *user;
@property (nonatomic , retain) IBOutlet UIView * headView;
@property (nonatomic , retain) IBOutlet UITextField * nameTF;

-(id)initWithUser:(UserEntity *)_user;
@end
