//
//  manageViewController.m
//  lertViewController
//
//  Created by qoo hudsioo on 3/11/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import "manageViewController.h"
#import "AppDelegate.h"
#import "freindRequestViewController.h"
#import "friendManageViewController.h"
#import "UserEntity.h"
#import "XMLConverter.h"
#import "ASIFormDataRequest.h"
#import "groupManageViewController.h"

@implementation manageViewController

@synthesize appDelegate;
@synthesize user;
@synthesize requestArray;

-(AppDelegate*)appDelegate{
	return (AppDelegate*)[[UIApplication sharedApplication]delegate];
}
-(id)initWithUser:(UserEntity *)_user{
    self = [super initWithNibName:@"manageViewController" bundle:nil];
    if (self) {
        self.user = _user;
        self.title = NSLocalizedString(@"Manage", @"Manage");
        self.tabBarItem.image = [UIImage imageNamed:@"112-group.png"];
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = NSLocalizedString(@"Manage", @"Manage");
        self.tabBarItem.image = [UIImage imageNamed:@"112-group.png"];
    }
    return self;
}


- (void)backToAddIn{
    NSLog(@"Back");
    [self.navigationController dismissModalViewControllerAnimated:YES];
}


-(void)insertDataFormXML:(id)xmlResult{
    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        NSLog(@"xmlResult = %@",xmlResult);
        // เผื่อเอา array มาทำอะไร
        
        // Save the context.
        self.requestArray = [xmlResult objectForKey:@"requests"];
        NSLog(@"requestArray = %@",[[xmlResult objectForKey:@"requests"] class]);
          [self tableView:self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [self.tableView reloadData];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
        NSIndexSet *indexset = [NSIndexSet indexSetWithIndex:0];
        [self.tableView reloadSections:indexset withRowAnimation:UITableViewRowAnimationNone];
    }
    
}


-(void)setupData{
   
    NSString *pathUrl = [NSString stringWithFormat:@"%@/user/requests/list",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                        <lertParams type='ilertu.user.requests-list'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>0</lastedUpdateDate>                        <language>%@</language>                        </common>                        </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,Language];
    
    
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
        XMLConverter *xml = [[XMLConverter alloc] init];

        
        NSLog(@"request responseStringn = %@",responseString);
    
        
        
        [self insertDataFormXML:[xml parseWithString:responseString]];
      //  [inDicView stopAnimating];
        [self.navigationController popViewControllerAnimated:YES];    
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *lbarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Add In" style:    
                                     UIBarButtonSystemItemUndo target:self 
                                                                   action:@selector(backToAddIn)];
    self.navigationItem.leftBarButtonItem = lbarButtonItem;
    

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupData];

    [self.tableView reloadData];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
    [self tableView:self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    if (section == 1) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                       reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
     
        
        if ([indexPath section] == 0) {
            if (indexPath.row == 0) {
                cell.textLabel.text = @"Requests";
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%d pending",self.requestArray.count];
                NSLog(@"%d",self.requestArray.count);
               
            }
       
        
        }
        if([indexPath section] == 1){
            if (indexPath.row == 0) {
                cell.textLabel.text = @"Friends";
            }else if(indexPath.row == 1){
                cell.textLabel.text = @"Manage groups of friends";
            }
        
        }
    
    
    }
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section]==0) {
        NSLog(@"count %d",[self.requestArray count]);
        if ([self.requestArray count] > 0) {
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:self.requestArray];

        
            freindRequestViewController * frRequestVC = [[freindRequestViewController alloc]initWithArray:tmpArray andUser:self.user];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
       
            [self.navigationController pushViewController:frRequestVC animated:YES];
             [frRequestVC release];
        }
        else {
             [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
        
       
    }
    else if ([indexPath section] == 1) {
        if (indexPath.row == 0) {
            friendManageViewController * frManageVC = [[friendManageViewController alloc]initWithUser:self.user];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [self.navigationController pushViewController:frManageVC animated:YES];
            [frManageVC release];
         }else if (indexPath.row == 1) {
            groupManageViewController * groupVC = [[groupManageViewController alloc]initWithUser:self.user];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [self.navigationController pushViewController:groupVC animated:YES];
        }
    }
   
     
}

@end
