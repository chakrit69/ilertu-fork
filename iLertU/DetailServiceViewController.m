//
//  DetailServiceViewController.m
//  iLertU
//
//  Created by Chakrit Paniam on 4/11/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "DetailServiceViewController.h"
#import "UserEntity.h"
#import "ASIFormDataRequest.h"
#import "XMLConverter.h"
#import "WebViewController.h"

@interface DetailServiceViewController ()

@end

@implementation DetailServiceViewController

@synthesize serviceArray;
@synthesize user;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)insertAddFormXML:(id)xmlResult{
    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        

        self.serviceArray = [xmlResult objectForKey:@"services"];
        [self.tableView reloadData];
    }
    
    
}

-(void)getServiceWithCatId:(NSString *)_catId{
    NSString *pathUrl = [NSString stringWithFormat:@"%@/services/search",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?><lertParams type='ilertu.services-search'>                        <common>                        <device>                        <deviceType>ios</deviceType>             <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>0</lastedUpdateDate>                        <language>%@</language>                        </common>                        <keywords>                        <categoryId>%@</categoryId>                        <serviceKeyword></serviceKeyword>                        <lastPageNumber></lastPageNumber>                        </keywords>                        </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,Language,_catId];
    
    
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
        
        XMLConverter *xml = [[XMLConverter alloc] init];
        
        [self insertAddFormXML:[xml parseWithString:responseString]];
        
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    

}
-(id)initWithUser:(UserEntity *)_user CatID:(NSString *)_catid{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        self.user = _user;
    }
    [self getServiceWithCatId:_catid];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Cat Service";

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return self.serviceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    // Configure the cell...
    cell.accessoryType = 1;
    
    cell.textLabel.text = [[self.serviceArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.detailTextLabel.text =[[self.serviceArray objectAtIndex:indexPath.row] objectForKey:@"desc"];

    NSURL* aURL = [NSURL URLWithString:[[self.serviceArray objectAtIndex:indexPath.row] objectForKey:@"imageUrl"]];
    NSData* data = [[NSData alloc] initWithContentsOfURL:aURL];
    cell.imageView.image = [UIImage imageWithData:data];

    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    NSString *path = [NSString stringWithFormat:@"http://202.60.194.200/ilertu_addins/ClientWeb/0_1/Service/Add/%@?serviceId=%@&lang=%@&deviceType=ios&deviceId=%@&currentVersion=1.0a",self.user.idUser,[[self.serviceArray objectAtIndex:indexPath.row] objectForKey:@"id"],Language,[[UIDevice currentDevice] uniqueIdentifier]];
    
    WebViewController *webView = [[WebViewController alloc] initWithURL:path];
    [self.navigationController pushViewController:webView animated:YES];

}

@end
