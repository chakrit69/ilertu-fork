//
//  Service.h
//  iLertU
//
//  Created by Chakrit Paniam on 4/6/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Service : NSManagedObject

@property (nonatomic, retain) NSString * active;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * imageUrl;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * responseStatus;

@end
