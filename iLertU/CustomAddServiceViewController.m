//
//  CustomAddServiceViewController.m
//  iLertU
//
//  Created by Chakrit Paniam on 3/11/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "CustomAddServiceViewController.h"

@implementation CustomAddServiceViewController

@synthesize leftSideButton=mLeftSideButton;
@synthesize rightSideButton=mRightSideButton;
@synthesize middleSideLabel=mMiddleSideLabel;
@synthesize middleSideButton=mMiddleSideButton;
@synthesize leftSideLabel=mLeftSideLabel;
@synthesize rightSideLabel=mRightSideLabel;



- (void)dealloc
{
    [mLeftSideButton release];
    [mRightSideButton release];
    [mMiddleSideButton release];
    [mLeftSideLabel release];
    [mRightSideLabel release];
    [mMiddleSideLabel release];
    
    [super dealloc];
}


#pragma mark - View lifecycle


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



@end
