//
//  EditProfileViewController.m
//  iLertU
//
//  Created by Chakrit Paniam on 3/29/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "EditProfileViewController.h"

#import "ASIFormDataRequest.h"

#import "AppDelegate.h"

#import "NSDataAdditions.h"

#import "UserEntity.h"

#import "XMLConverter.h"

#import "AddInViewController.h"
#import "IndicatorView.h"


@implementation EditProfileViewController

@synthesize imagePicker;
@synthesize imgBtn;
@synthesize genderBtn;
@synthesize pickerBD;
@synthesize pickerGender;

@synthesize headerView;

@synthesize user;


@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;

-(void)dataFormXML:(id)xmlResult{
    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        [inDicView stopAnimating];
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        
        
        self.user.idUser = [[xmlResult objectForKey:@"user"] objectForKey:@"id"];
        self.user.name =   [[[xmlResult objectForKey:@"user"] objectForKey:@"name"] objectForKey:@"given"];
        self.user.salutation =[[xmlResult objectForKey:@"user"] objectForKey:@"salutation"];
        self.user.active = [[xmlResult objectForKey:@"user"] objectForKey:@"active"];
        self.user.surname = [[[xmlResult objectForKey:@"user"] objectForKey:@"name"] objectForKey:@"family"];
        self.user.idCard = [[xmlResult objectForKey:@"user"] objectForKey:@"idCard"];
        self.user.gender = [[xmlResult objectForKey:@"user"] objectForKey:@"gender"];
        self.user.birthday = [[xmlResult objectForKey:@"user"] objectForKey:@"birthday"];
        self.user.imageUrl = [[xmlResult objectForKey:@"user"] objectForKey:@"imageUrl"];
        self.user.phone = [[xmlResult objectForKey:@"user"] objectForKey:@"phone"];
        self.user.username = [[[xmlResult objectForKey:@"user"] objectForKey:@"authen"] objectForKey:@"username"];
        self.user.password = [[[xmlResult objectForKey:@"user"] objectForKey:@"authen"] objectForKey:@"password"];
        self.user.lastedUpdateDate =[xmlResult objectForKey:@"lastedUpdateDate"];
        
    }
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithUser:(UserEntity *)_user{
    self = [super initWithNibName:@"EditProfileViewController" bundle:nil];if (self) {
        self.user = _user;
       // [self getUserInformation];
    }
    return self;
}

//-(void)getUserInformation{
//    NSString *pathUrl = [NSString stringWithFormat:@"%@/user/get",ILERTU];
//    NSURL *url = [NSURL URLWithString:pathUrl];
//    
//    
//    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
//    
//    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
//    
//    
//    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
//    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
//    
//    NSString *Language;
//    Language = [plistDict objectForKey:@"Language"];
//    
//    
//    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?><lertParams type='ilertu.user.get'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen/>                        <lastedUpdateDate>%@</lastedUpdateDate>                        <language>%@</language>                        </common>                        <user id='%@'/>                        </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.lastedUpdateDate,Language,self.user.idUser];
//    
//    
//    
//    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    
//    [request2 setCompletionBlock:^{
//        NSString *responseString = [request2 responseString];
//        
//        XMLConverter *xml = [[XMLConverter alloc] init];
//        [xml parseWithString:responseString];
//        
//
//        
//        
//        [self dataFormXML:[xml parseWithString:responseString]];
//        
//        
//    }];
//    
//    [request2 setFailedBlock:^{
//        NSError *error = [request2 error];
//        NSLog(@"Error: %@", error.localizedDescription);
//        
//        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertMessageView show];
//        [alertMessageView release];
//        
//    }];
//    
//    [request2 startAsynchronous];
//    
//    
//    
//}


-(void)camera
{
    self.imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    [self presentModalViewController:self.imagePicker animated:YES];
    
}

-(void)imageFormGal
{
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentModalViewController:self.imagePicker animated:YES];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.imagePicker dismissModalViewControllerAnimated:YES];
    UIImage *img = [info valueForKey:UIImagePickerControllerEditedImage];
    [self.imgBtn setImage:img forState:UIControlStateNormal];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.imagePicker dismissModalViewControllerAnimated:YES];
    
}



-(void)insertDataFormXML:(id)xmlResult{
    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        [inDicView stopAnimating];
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
        // NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
        
        UserEntity *newUser = (UserEntity *)[NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
        newUser.idUser = [[xmlResult objectForKey:@"user"] objectForKey:@"id"];
        newUser.name =   [[[xmlResult objectForKey:@"user"] objectForKey:@"name"] objectForKey:@"given"];
        newUser.salutation =[[xmlResult objectForKey:@"user"] objectForKey:@"salutation"];
        newUser.active = [[xmlResult objectForKey:@"user"] objectForKey:@"active"];
        newUser.surname = [[[xmlResult objectForKey:@"user"] objectForKey:@"name"] objectForKey:@"family"];
        newUser.idCard = [[xmlResult objectForKey:@"user"] objectForKey:@"idCard"];
        newUser.gender = [[xmlResult objectForKey:@"user"] objectForKey:@"gender"];
        newUser.birthday = [[xmlResult objectForKey:@"user"] objectForKey:@"birthday"];
        newUser.imageUrl = [[xmlResult objectForKey:@"user"] objectForKey:@"imageUrl"];
        newUser.phone = [[xmlResult objectForKey:@"user"] objectForKey:@"phone"];
        newUser.username = [[[xmlResult objectForKey:@"user"] objectForKey:@"authen"] objectForKey:@"username"];
        newUser.password = [[[xmlResult objectForKey:@"user"] objectForKey:@"authen"] objectForKey:@"password"];
        newUser.lastedUpdateDate =[xmlResult objectForKey:@"lastedUpdateDate"];
        
        
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        
        AddInViewController *addInView = [[AddInViewController alloc] initWithUser:user];
        UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:addInView] autorelease];
        [self.navigationController presentModalViewController:navController animated:YES];
        [addInView release];
        
        
    }
    
}


-(void)DoneSendData{
    [inDicView startAnimating];
    
    
if (nameTF.text != NULL && surnameTF.text != NULL && idCardTF.text != NULL && mobileTF.text != NULL ) {
        
        NSData *imageData = UIImageJPEGRepresentation(self.imgBtn.imageView.image, 1.0);
        NSString *imageBase64String = [imageData base64Encoding];
        
        NSString *pathUrl = [NSString stringWithFormat:@"%@/user/update",ILERTU];
        NSURL *url = [NSURL URLWithString:pathUrl];
        
        
        ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
        
        [request2 addRequestHeader:@"content-type" value:@"text/xml"];
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
        NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
        
        NSString *Language;
        Language = [plistDict objectForKey:@"Language"];
        
        
        
        NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                            <lertParams type='ilertu.user.update'><common><device><deviceType>ios</deviceType>                            <deviceId>%@</deviceId><currentVersion>1.0a</currentVersion></device><authen>                               <username>%@</username><password>%@</password></authen><lastedUpdateDate>%@</lastedUpdateDate> <language>%@</language> </common>                            <user id='%@'>                           <idCard>%@</idCard>                            <salutation>%@</salutation>                            <name>                           <given>%@</given>                            <family>%@</family>                           </name>                           <phone>%@</phone>                            <birthday>%@</birthday>                            <image>                            <imageSource>%@</imageSource>                            <postType>1</postType>                            </image>                            </user></lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,self.user.lastedUpdateDate,Language,self.user.idUser,idCardTF.text,self.genderBtn.titleLabel.text,nameTF.text,surnameTF.text,mobileTF.text,personLB.text,imageBase64String];
        
        
        
        [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [request2 setCompletionBlock:^{
            NSString *responseString = [request2 responseString];
            
            XMLConverter *xml = [[[XMLConverter alloc] init] autorelease];
            
            [self insertDataFormXML:[xml parseWithString:responseString]];
            [inDicView stopAnimating];
            [self.navigationController popViewControllerAnimated:YES];    
            
        }];
        
        [request2 setFailedBlock:^{
            NSError *error = [request2 error];
            NSLog(@"Error: %@", error.localizedDescription);
            
            UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertMessageView show];
            [alertMessageView release];
            
        }];
        
        [request2 startAsynchronous];
        

    }
    else{
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:@"Please Insert data" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    
    }    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (__managedObjectContext == nil) 
    { 
        __managedObjectContext = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext]; 
    }
    
    self.title = @"pro file";
    UIBarButtonItem *barButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:    
                                    UIBarButtonItemStylePlain target:self 
                                                                  action:@selector(DoneSendData)];

    self.navigationItem.rightBarButtonItem = barButtonItem; 

    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = YES;

    self.tableView.tableHeaderView = self.headerView;
    self.tableView.sectionHeaderHeight = 3.0;
    self.tableView.sectionFooterHeight = 3.0;
    
    genderArray = [[NSArray alloc]initWithObjects:@"Mr.",@"Miss",@"Mrs", nil];
   
   

    
    nameTF = [[UITextField alloc] initWithFrame:CGRectMake(15, 10, 180, 30)];
    nameTF.adjustsFontSizeToFitWidth = YES;
    nameTF.textColor = [UIColor blackColor];
    
    
    surnameTF = [[UITextField alloc] initWithFrame:CGRectMake(15, 10, 180, 30)];
    surnameTF.adjustsFontSizeToFitWidth = YES;
    surnameTF.textColor = [UIColor blackColor];
    
    
    idCardTF = [[UITextField alloc] initWithFrame:CGRectMake(15, 10, 180, 30)];
    idCardTF.adjustsFontSizeToFitWidth = YES;
    idCardTF.textColor = [UIColor blackColor];
    
    
    mobileTF = [[UITextField alloc] initWithFrame:CGRectMake(110, 10, 180, 30)];
    mobileTF.adjustsFontSizeToFitWidth = YES;
    mobileTF.textColor = [UIColor blackColor];
    
    
    
    personLB = [[UILabel alloc] initWithFrame:CGRectMake(110, 10, 100, 30)];
    personLB.adjustsFontSizeToFitWidth = YES;
    personLB.textColor = [UIColor blackColor];

    inDicView = [[IndicatorView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    [self.view addSubview:inDicView];
    [inDicView startAnimating];

}

- (void)viewDidAppear:(BOOL)animated{
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.user.imageUrl]];
    [self.imgBtn setImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
    nameTF.text = self.user.name;
    surnameTF.text = self.user.surname;
    idCardTF.text = self.user.idCard;
    mobileTF.text = self.user.phone;
    genderBtn.titleLabel.text = self.user.salutation;
    
    [inDicView stopAnimating];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    if (section == 0) {
        return 3;
    }else if(section == 1){
        return 1;
    }else if(section == 2){
        return 1;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
                                       reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        
        
        
        if ([indexPath section] == 0) {
            
            if ([indexPath row] == 0) {
                nameTF.placeholder = @"Name";
                nameTF.keyboardType = UIKeyboardTypeDefault;
                nameTF.returnKeyType = UIReturnKeyDefault;
                [cell addSubview:nameTF];
                
            }else if([indexPath row] == 1) {
                surnameTF.placeholder = @"Surname";
                surnameTF.keyboardType = UIKeyboardTypeDefault;
                surnameTF.returnKeyType = UIReturnKeyDefault;
                [cell addSubview:surnameTF];
            }else {
                idCardTF.placeholder = @"ID Card";
                idCardTF.keyboardType = UIKeyboardTypeDefault;
                idCardTF.returnKeyType = UIReturnKeyDefault;
                [cell addSubview:idCardTF];
            }
            
            
        }else if ([indexPath section] == 1) {
            if ([indexPath row] == 0) {
                mobileTF.placeholder = @"Phone";
                mobileTF.keyboardType = UIKeyboardTypePhonePad;
                mobileTF.returnKeyType = UIReturnKeyDefault;
                [cell addSubview:mobileTF];
            }
            
            
        }else if ([indexPath section] == 2) {
            if ([indexPath row] == 0) {
                personLB.textColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.5];
                NSString * bdString;
                bdString = self.user.birthday;
                bdString = [bdString stringByReplacingOccurrencesOfString:@"0" withString:@""];
                bdString = [bdString stringByReplacingOccurrencesOfString:@" " withString:@""];
                bdString = [bdString stringByReplacingOccurrencesOfString:@" " withString:@""];
                personLB.text = self.user.birthday;
                
            }
            personLB.backgroundColor = [UIColor whiteColor];
            personLB.textAlignment = UITextAlignmentLeft;
            
            [personLB setEnabled: YES];
            [personLB setUserInteractionEnabled:YES];
            
            [cell addSubview:personLB];
            [personLB release];
            
        }        
    }
    
    
    
    
    if ([indexPath section] == 0) {
        
    }else if([indexPath section] == 1) {
        cell.textLabel.text = @"mobile";
    }else if([indexPath section] == 2) {
        cell.textLabel.text = @"birthday";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    
    
    return cell; 

    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
- (void)DoneClick:(id)sender{
    [menuGender dismissWithClickedButtonIndex:0 animated:YES];
}
- (void)DoneDateClick:(id)sender{
    [menuBD dismissWithClickedButtonIndex:0 animated:YES];
}
- (void)dateChange{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd/MM"];
    NSDateFormatter *yearFormatter = [[NSDateFormatter alloc]init];
    [yearFormatter setDateFormat:@"yyyy"];
    NSString *selectionDate = [dateFormatter stringFromDate:[pickerBD date]];
    NSString *selectionYear = [yearFormatter stringFromDate:[pickerBD date]];
    int  year = [selectionYear intValue];
    
    if (year > 2200) {
        
        year = year-543;
    }
    NSString * comDate = [NSString stringWithFormat:@"%@/%d",selectionDate,year];
    personLB.text = comDate;
    [yearFormatter release];
    [dateFormatter release];
    
}

- (IBAction)toggleView:(id)sender {	
	
	UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:nil
													  delegate:self
											 cancelButtonTitle:@"Cancel"
										destructiveButtonTitle:NULL
											 otherButtonTitles:@"Photo Libary" ,@"Take Camera",nil];
	
    
	[menu showInView:self.view];
	//[menu setBounds:CGRectMake(0,0,320, 260)];
	[menu release];
}



- (IBAction)toggleGender:(id)sender{
    menuGender = [[UIActionSheet alloc] initWithTitle:nil
                                             delegate:self
                                    cancelButtonTitle:nil
                               destructiveButtonTitle:NULL
                                    otherButtonTitles:nil];
	// Add the picker
	self.pickerGender = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,0,0)];
	self.pickerGender.delegate = self;
	self.pickerGender.showsSelectionIndicator = YES; // note this is default to NO
    
    UIToolbar * doneToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    doneToolBar.barStyle = UIBarStyleBlackOpaque;
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DoneClick:)];
    [barItems addObject:doneBtn];
    
    [doneToolBar setItems:barItems animated:YES];
    
    
    [menuGender addSubview:doneToolBar];
	[menuGender addSubview:self.pickerGender];
	[menuGender showInView:self.view];
	[menuGender setBounds:CGRectMake(0,0,320, 500)];
    
    
}

- (IBAction)toggleBD {	
	
    menuBD = [[UIActionSheet alloc] initWithTitle:nil
                                         delegate:self
                                cancelButtonTitle:nil
                           destructiveButtonTitle:NULL
                                otherButtonTitles:nil];
	// Add the picker

	self.pickerBD = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,44,0,0)];
    self.pickerBD.datePickerMode = UIDatePickerModeDate;
  
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    self.pickerBD.calendar = gregorian;

    
    [self.pickerBD addTarget:self action:@selector(dateChange) forControlEvents:UIControlEventValueChanged];
    
    UIToolbar * doneToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    doneToolBar.barStyle = UIBarStyleBlackOpaque;
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DoneDateClick:)];
    [barItems addObject:doneBtn];
    
    [doneToolBar setItems:barItems animated:YES];
    
    [menuBD addSubview:doneToolBar];
	[menuBD addSubview:self.pickerBD];
	[menuBD showInView:self.view];
	[menuBD setBounds:CGRectMake(0,0,320, 500)];
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (actionSheet == menuGender) {
        
        
    }else if(actionSheet == menuBD){
        
    }else{
        if(buttonIndex==0){
            [self imageFormGal];
        }else if(buttonIndex==1){
            [self camera];
            
        }
    }
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	if (pickerView == self.pickerGender) {
        return 3;
    }
    return 0;
	
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	
	return [genderArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.genderBtn.titleLabel.text = [genderArray objectAtIndex:row];
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([indexPath section] == 2) {
        [self toggleBD];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
    }else{
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (__fetchedResultsController != nil) {
        return __fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserEntity" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"name" ascending:NO] autorelease];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"] autorelease];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return __fetchedResultsController;
}   


@end
