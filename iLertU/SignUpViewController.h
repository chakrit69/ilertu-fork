//
//  SignUpViewController.h
//  iLertU
//
//  Created by Chakrit Paniam on 3/10/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IndicatorView.h"
@class IndicatorView;
@interface SignUpViewController :UITableViewController<UIPickerViewDelegate, UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UITextFieldDelegate,NSFetchedResultsControllerDelegate>{
    
    NSArray * genderArray;
    UIActionSheet * menuGender;
    UIActionSheet * menuBD;
    UILabel * personLB;
    
    
    UITextField * nameTF;
    UITextField * surnameTF;
    UITextField * idCardTF;
    UITextField * mobileTF;
    UITextField * emailTF;
    UITextField * passTF;
    UITextField * conTF;
    IndicatorView *inDicView;
}


@property (nonatomic, retain) UIImagePickerController * imagePicker;
@property (nonatomic, retain) IBOutlet UIButton * imgBtn;
@property (nonatomic, retain) UIDatePicker * pickerBD;
@property (nonatomic, retain) UIPickerView * pickerGender;

@property (nonatomic, retain) IBOutlet UIView* headerView;
@property (nonatomic, retain) IBOutlet UIButton* genderBtn;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;



-(void)camera;
-(void)imageFormGal;
- (IBAction)toggleView:(id)sender;
- (IBAction)toggleGender:(id)sender;
- (IBAction)toggleBD;

@end
