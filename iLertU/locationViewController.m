//
//  FirstViewController.m
//  lertViewController
//
//  Created by qoo hudsioo on 3/11/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import "locationViewController.h"
#import "filterViewController.h"
#import "AppDelegate.h"
#import "IndicatorView.h"
#import "XMLConverter.h"
#import "ASIFormDataRequest.h"
#import "UserEntity.h"

@interface AddressAnnotation : NSObject<MKAnnotation> {
    CLLocationCoordinate2D coordinate;
    NSString *mTitle;
    NSString *mSubTitle;
}
@property (nonatomic,retain) NSString * mTitle;
@property (nonatomic,retain) NSString * mSubTitle;
@end

@implementation AddressAnnotation

@synthesize coordinate,mTitle,mSubTitle;

- (NSString *)subtitle{
    return self.mTitle;
}

- (NSString *)title{
    return self.mSubTitle;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c titlex:(NSString*)t subtitlex:(NSString*)s{
    coordinate=c;   
    self.mTitle = t;
    self.mSubTitle = s;

    NSLog(@"%f,%f",c.latitude,c.longitude);
    NSLog(@"%@",self.mTitle);
    return self;
}
@end


@implementation locationViewController
@synthesize navBar;
@synthesize mapView;
@synthesize appDelegate;
@synthesize friendArray;
@synthesize user;
-(AppDelegate*)appDelegate{
	return (AppDelegate*)[[UIApplication sharedApplication]delegate];
}
-(id)initWithUser:(UserEntity *)_user{
    self = [super initWithNibName:@"locationViewController" bundle:nil];
    if (self) {
        self.user = _user;
        self.title = @"Lerts monitor";
        self.tabBarItem.title = @"Location";
        self.tabBarItem.image = [UIImage imageNamed:@"13-target.png"];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      //  self.title = NSLocalizedString(@"Lerts monitor", @"Location");
        self.title = @"Lerts monitor";
        self.tabBarItem.title = @"Location";
        self.tabBarItem.image = [UIImage imageNamed:@"13-target.png"];
    }
    return self;
}
		

-(void)insertDataFormXML:(id)xmlResult{


    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
      //  NSLog(@"xmlResult = %@",xmlResult);

        self.friendArray = [xmlResult objectForKey:@"friends"];
        if ([self.friendArray count] > 0) {
            [acView startAnimating];
            self.navigationItem.rightBarButtonItem.enabled = YES;
        }else {
            self.navigationItem.rightBarButtonItem.enabled = NO;
        }
    }
    
}


-(void)setupData{
    
    NSString *pathUrl = [NSString stringWithFormat:@"%@/user/friends/list",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                        <lertParams type='ilertu.user.friends-list'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>0</lastedUpdateDate>                        <language>%@</language>                        </common>                        </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,Language];
    
    
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
        XMLConverter *xml = [[XMLConverter alloc] init];
        
        
        NSLog(@"request responseStringn = %@",responseString);
        
        
        
        [self insertDataFormXML:[xml parseWithString:responseString]];
        //  [inDicView stopAnimating];
        // [self.navigationController popViewControllerAnimated:YES];    
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    
}


-(void)pushView{

    filterViewController * filterVC = [[filterViewController alloc]initWithUser:self.user andFriendArray:self.friendArray];
    [self.navigationController pushViewController:filterVC animated:YES];
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
	MKPinAnnotationView *annotationView = [[[MKPinAnnotationView alloc]
											initWithAnnotation:annotation 
											reuseIdentifier:@"iLertU"] autorelease];

     if (annotation == self.mapView.userLocation) return nil;
	annotationView.canShowCallout = YES;
	annotationView.opaque = NO;
    
	UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
	annotationView.rightCalloutAccessoryView = rightButton;
    [acView stopAnimating];
	return annotationView;
}






-(void)setupMapView{
	MKCoordinateSpan span;
	span.latitudeDelta = 0.05;
	span.longitudeDelta = 0.05;
	MKCoordinateRegion region;
	region.span = span;
    CLLocationCoordinate2D centerCoord = {13.822, 100.01 };
	region.center = centerCoord;
   // NSLog(@"region = %.5f , %.5f",region.center.latitude ,region.center.longitude);
	[self.mapView setRegion:region animated:YES];
    //[self.mapView removeAnnotations:self.mapView.annotations];
	// [self.mapView addAnnotations:self.appDelegate.trackArray];
        
	self.mapView.delegate = self;
    [acView stopAnimating];
    //[acView removeFromSuperview];
    //[acView release];

}

- (void)backToAddIn{
    NSLog(@"Back");
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationItem.title = @"xxx";	// Do any additional setup after loading the view, typically from a nib.
    UIBarButtonItem *rbarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Filter" style:    
                                    UIBarButtonItemStylePlain target:self 
                                                                  action:@selector(pushView)];
    
    UIBarButtonItem *lbarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Add In" style:    
                                    UIBarButtonSystemItemUndo target:self 
                                                                  action:@selector(backToAddIn)];
    
    self.navigationItem.rightBarButtonItem = rbarButtonItem;
    self.navigationItem.leftBarButtonItem = lbarButtonItem;
   // self.navigationItem.rightBarButtonItem.enabled = NO;
    
    [self setupData];
     NSLog(@"fr count= %d",[self.friendArray count]);
    
    acView = [[IndicatorView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)]; 
    [self.mapView addSubview:acView];
    self.mapView.showsUserLocation = YES;
    
    [self performSelector:@selector(setupMapView) withObject:nil afterDelay:4.0];

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //self.mapView.showsUserLocation = self.appDelegate.currectStatus;
    
    [self setupMapView];
    NSLog(@"fr count= %d",[self.friendArray count]);
    
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


@end
