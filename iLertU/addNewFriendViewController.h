//
//  addNewFriendViewController.h
//  iLertU
//
//  Created by qoo hudsioo on 4/10/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserEntity;

@interface addNewFriendViewController : UIViewController{
    
}

@property (nonatomic, retain) UserEntity* user;

@property (nonatomic, retain) IBOutlet UITextField * emailF;
@property (nonatomic, retain) IBOutlet UITextField * phoneF;


-(id)initWithUser:(UserEntity *)_user;
-(IBAction)sendRequest:(id)sender;

@end
