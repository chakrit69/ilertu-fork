//
//  UserEntity.h
//  iLertU
//
//  Created by Chakrit Paniam on 4/6/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UserEntity : NSManagedObject

@property (nonatomic, retain) NSString * active;
@property (nonatomic, retain) NSString * birthday;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSString * idCard;
@property (nonatomic, retain) NSString * idUser;
@property (nonatomic, retain) NSString * imageUrl;
@property (nonatomic, retain) NSString * lastedUpdateDate;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * salutation;
@property (nonatomic, retain) NSString * surname;
@property (nonatomic, retain) NSString * username;

@end
