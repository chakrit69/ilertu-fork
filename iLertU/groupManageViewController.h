//
//  groupManageViewController.h
//  iLertU
//
//  Created by qoo hudsioo on 4/1/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UserEntity;
@interface groupManageViewController : UITableViewController{
    
}
@property (nonatomic , retain) NSMutableArray * groupsArray;
@property (nonatomic , retain) UserEntity *user;
-(id)initWithUser:(UserEntity *)_user;
@end
