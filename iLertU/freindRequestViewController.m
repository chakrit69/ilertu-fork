//
//  freindRequestViewController.m
//  lertViewController
//
//  Created by qoo hudsioo on 3/14/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import "freindRequestViewController.h"
#import "AppDelegate.h"
#import "UserEntity.h"
#import "XMLReader.h"
#import "ASIFormDataRequest.h"
#import "XMLConverter.h"



@implementation freindRequestViewController
@synthesize appDelegate;
@synthesize user;
@synthesize frRequest;
@synthesize showName;

-(AppDelegate*)appDelegate{
	return (AppDelegate*)[[UIApplication sharedApplication]delegate];
}

- (id)initWithArray:(NSMutableArray*)reFriend andUser:(UserEntity *)_user{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        self.frRequest = [[NSMutableArray alloc] init];
        self.frRequest = reFriend;
        self.user = _user;
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    fr = [[NSDictionary alloc]init];
    self.title = @"Requests";


    frIndex = [[NSMutableArray alloc] init];
    for (NSDictionary * i in self.frRequest){
        char alpha = [[i objectForKey:@"name"] characterAtIndex:0];
      //  char alphabet = [[listOfStates objectAtIndex:i] characterAtIndex:0];
        NSString *uniChar = [NSString stringWithFormat:@"%C", alpha];

        if (![frIndex containsObject:uniChar])
        {
            [frIndex addObject:uniChar];
        }
    }
    [frIndex sortUsingSelector:@selector(compare:)];
    
    frName = [[NSMutableArray alloc]init];
    for (NSDictionary * i in self.frRequest){
        [frName addObject:[i objectForKey:@"name"]];

    }
    [frName sortUsingSelector:@selector(compare:)];
    
    NSLog(@"Class %@",[[frRequest objectAtIndex:0]class]);


}

- (void)viewDidUnload
{
    [super viewDidUnload];
 
}
-(void)insertDataFormXML:(id)xmlResult{
    fr = [[NSDictionary alloc]init];
    self.frRequest = [[NSMutableArray alloc]init];
    frName = [[NSMutableArray alloc]init];
    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        NSLog(@"xmlResult = %@",xmlResult);
        self.showName = [[NSArray alloc]init];
        self.frRequest = [xmlResult objectForKey:@"requests"];
        if (self.frRequest != NULL) {
            
            for (NSDictionary * i in self.frRequest){
                char alpha = [[i objectForKey:@"name"] characterAtIndex:0];
                //  char alphabet = [[listOfStates objectAtIndex:i] characterAtIndex:0];
                NSString *uniChar = [NSString stringWithFormat:@"%C", alpha];
                
                if (![frIndex containsObject:uniChar])
                {
                    [frIndex addObject:uniChar];
                }
            }
            [frIndex sortUsingSelector:@selector(compare:)];
            for (NSDictionary * i in self.frRequest){
                [frName addObject:[i objectForKey:@"name"]];
                
            }
            [frName sortUsingSelector:@selector(compare:)];
        }
        
        
        NSLog(@"requestArray = %@",[[xmlResult objectForKey:@"requests"] class]);
        [self.tableView reloadData];
        [self tableView:self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    
}



-(void)sendRequest:(NSString*)answer{
    
    NSString *pathUrl = [NSString stringWithFormat:@"%@/user/requests/response",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                        <lertParams type='ilertu.user.requests-response'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>0</lastedUpdateDate>                        <language>%@</language>                        </common>                        <request id='%@'>                        <accept>%@</accept>                        </request>                        </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,Language,[fr objectForKey:@"id"],answer];
    
    
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request2 setCompletionBlock:^{
        
        NSString *responseString = [request2 responseString];
        
        XMLConverter *xml = [[XMLConverter alloc] init];
        
        
        NSLog(@"request responseStringn = %@",responseString);
        
        
        
        [self insertDataFormXML:[xml parseWithString:responseString]];
        
      //  [frRequest removeObjectAtIndex:indexF];
        //[frName removeObjectAtIndex  :indexF];

        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView == alertViewAccept) {
        if (buttonIndex == 0) {
  
        } else if(buttonIndex == 1){
            [self sendRequest:@"true"];
            [self.tableView reloadData];
        }
    }
    if (alertView == alertViewReject) {
        if (buttonIndex == 0) {

        } else if(buttonIndex == 1){
            [self sendRequest:@"false"];
            [self.tableView reloadData];
        }
    }
    
	
}


-(void)toggleAccept:(NSDictionary*)frDicRequest{
    UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Actons for %@",[frDicRequest objectForKey:@"name"]]
													  delegate:self
											 cancelButtonTitle:@"Cancel"
										destructiveButtonTitle:NULL
											 otherButtonTitles:@"Accept" ,@"Reject",nil];
	
    
	[menu showInView:self.view];
	//[menu setBounds:CGRectMake(0,0,320, 260)];
	[menu release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0) {
        alertViewAccept = [[UIAlertView alloc] initWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Do you want to accept '%@'",[fr objectForKey:@"name"]] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alertViewAccept show];
        [alertViewAccept release];
        
  
    }else if(buttonIndex == 1){
        alertViewReject = [[UIAlertView alloc] initWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Do you want to reject '%@'",[fr objectForKey:@"name"]] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alertViewReject show];
        [alertViewReject release];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [frIndex count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString * alpha = [frIndex objectAtIndex:section];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c]%@",alpha];
    NSArray *frx = [frName filteredArrayUsingPredicate:predicate];
    return [frx count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                       reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
       
    }
    NSString * alpha = [frIndex objectAtIndex:[indexPath section]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c]%@", alpha];
    self.showName = [frName filteredArrayUsingPredicate:predicate];

    
    if ([self.showName count] >0) {
        NSString * cellText = [self.showName objectAtIndex:indexPath.row];
        cell.textLabel.text = cellText;
    }
    
    return cell;
}


- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section {
	return [frIndex objectAtIndex:section];
    
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return frIndex;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * alpha = [frIndex objectAtIndex:[indexPath section]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c]%@", alpha];
    NSArray *frx = [frName filteredArrayUsingPredicate:predicate];
    int count=0;
    for (NSDictionary *  i in self.frRequest ){
        if ([[frx objectAtIndex:indexPath.row] isEqualToString:[i objectForKey:@"name"]]) {
            [self toggleAccept:i];
            fr = [self.frRequest objectAtIndex:count];
            indexF = count;
            
        }
        count++;
    }
}

@end
