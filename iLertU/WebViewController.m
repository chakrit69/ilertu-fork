//
//  WebViewController.m
//  iLertU
//
//  Created by Chakrit Paniam on 3/9/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "WebViewController.h"
#import "IndicatorView.h"

@implementation WebViewController

@synthesize webView;
@synthesize webURL;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithURL:(NSString *)url{
    self = [super init];
    if (self) {
        self.webURL = url;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    inDicView = [[IndicatorView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];  
    [self.view addSubview:inDicView];
    self.webView.delegate = self;
    NSURL *url = [NSURL URLWithString:self.webURL];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [webView loadRequest:requestObj];
    // Do any additional setup after loading the view from its nib.
   // [inDicView startAnimating];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	// starting the load, show the activity indicator in the status bar
    NSLog(@"start web");
    [inDicView startAnimating];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
     NSLog(@"stop web");
	// finished loading, hide the activity indicator in the status bar
    [inDicView stopAnimating];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
