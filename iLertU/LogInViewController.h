//
//  LogInViewController.h
//  iLertU
//
//  Created by Chakrit Paniam on 2/28/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBConnect.h"
#import "AppDelegate.h"
@class IndicatorView;
@interface LogInViewController : UIViewController<FBRequestDelegate,FBDialogDelegate,FBSessionDelegate,NSFetchedResultsControllerDelegate>{
    IndicatorView * inDicView;
    
}
@property (nonatomic, retain)IBOutlet UIButton* loginWithFBButton;
@property (nonatomic, retain)IBOutlet UIButton* signInButton;
@property (nonatomic, retain)IBOutlet UIButton* signUpButton;
@property (nonatomic, retain)IBOutlet UIButton* skipButton;


@property (nonatomic, retain) UIActivityIndicatorView *activityIndicator;

@property (nonatomic, retain) NSArray *permissions;

@property (nonatomic, retain) NSMutableDictionary * plistDict;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, retain)AppDelegate *appDelegate;


-(IBAction)loginWithFBButtonTapped:(id)sender;
-(IBAction)signIn:(id)sender;
-(IBAction)signUP:(id)sender;
-(IBAction)skipButtonTapped:(id)sender;

@end
