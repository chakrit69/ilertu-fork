//
//  groupManageViewController.m
//  iLertU
//
//  Created by qoo hudsioo on 4/1/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import "groupManageViewController.h"
#import "UserEntity.h"
#import "XMLReader.h"
#import "ASIFormDataRequest.h"
#import "XMLConverter.h"
#import "addNewGroupsViewController.h"

@implementation groupManageViewController
@synthesize groupsArray;
@synthesize user;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithUser:(UserEntity *)_user{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        self.user = _user;
    }
    return self;
}

-(void)insertDataFormXML:(id)xmlResult{

    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        NSLog(@"xmlResult = %@",xmlResult);
        self.groupsArray = [xmlResult objectForKey:@"groups"];

        
        NSLog(@"requestArray = %@",[[xmlResult objectForKey:@"groups"] class]);
        [self.tableView reloadData];
        [self tableView:self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    
}
-(void)setupData{
    
    NSString *pathUrl = [NSString stringWithFormat:@"%@/user/friendgroups/list",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                        <lertParams type='ilertu.user.friendgroups-list'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>0</lastedUpdateDate>                        <language>%@</language>                        </common>                        </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,Language];
    
    
    
                         [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
                         
                         
                         [request2 setCompletionBlock:^{
        
        NSString *responseString = [request2 responseString];
        
        XMLConverter *xml = [[XMLConverter alloc] init];
        
        
        NSLog(@"request responseStringn = %@",responseString);
        
        
        
        [self insertDataFormXML:[xml parseWithString:responseString]];
        
        //  [frRequest removeObjectAtIndex:indexF];
        //[frName removeObjectAtIndex  :indexF];
        
        
    }];
                         
                         [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
                         
                         [request2 startAsynchronous];
    
}

- (void)addNewGroups{
    addNewGroupsViewController * addNewGVC = [[addNewGroupsViewController alloc] initWithUser:self.user];
    [self.navigationController pushViewController:addNewGVC animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"groups";
    self.groupsArray = [[NSMutableArray alloc]init];
    [self setupData];
    
    UIBarButtonItem *rbarButtonItem=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewGroups)];    
    
    self.navigationItem.rightBarButtonItem = rbarButtonItem;

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [self.groupsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                       reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
