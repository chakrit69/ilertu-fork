//
//  XMLConverter.m
//  XMLConverter
//
//  Created by Katanyoo Ubalee on 4/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "XMLConverter.h"
#import "CXMLElement.h"
#import "CXMLDocument.h"


@implementation XMLConverter

- (int)uniqNameCountForChild:(CXMLElement *)child
{
    NSMutableSet *nameSet = [[NSMutableSet alloc] init];
    
    for (CXMLElement *subChild in [child children]) {
        if ([subChild isKindOfClass:[CXMLElement class]])
        {
            [nameSet addObject:[subChild name]];
        }
    }
    return [nameSet count];
}

- (id)parseChild:(CXMLElement *)child
{
    if ([self uniqNameCountForChild:child] == 1)
    {
        if ([[[[[child children] objectAtIndex:0] children] objectAtIndex:0] isKindOfClass:[CXMLElement class]]) {
            NSMutableArray *value = [[NSMutableArray alloc] init];
            for (CXMLElement *subChild in [child children]) {
                if ([subChild isKindOfClass:[CXMLElement class]])
                {
                    [value addObject:[self parseChild:subChild]];
                }
            }
            return value;
        }
        else if ([child childCount] == 1 && [[[[[child children] objectAtIndex:0] children] objectAtIndex:0] isKindOfClass:[CXMLNode class]]) {
            NSMutableDictionary *value = [[NSMutableDictionary alloc] init];
            if ([[child attributes] count] > 0) {
                for (CXMLNode *attr in [child attributes]) {
                    [value setObject:[attr stringValue] forKey:[attr name]];
                }
            }
            
            CXMLElement *subChild = [[child children] objectAtIndex:0];
            if ([[subChild attributes] count] > 0) {
                [value setObject:[self parseChild:subChild] forKey:[subChild name]];
            }
            else {
                
                [value setObject:[subChild stringValue] forKey:[subChild name]];
            }
            return value;
        }
    }
    else {
        NSMutableDictionary *value = [[NSMutableDictionary alloc] init];
        
        if ([[child attributes] count] > 0) {
            for (CXMLNode *attr in [child attributes]) {
                [value setObject:[attr stringValue] forKey:[attr name]];
            }
        }
        
        for (CXMLElement *subChild in [child children]) {
            if ([subChild isKindOfClass:[CXMLElement class]])
            {
                if ([subChild childCount] > 1 || [[subChild attributes] count] > 0 || [self uniqNameCountForChild:subChild] == 1)
                {
                    [value setObject:[self parseChild:subChild] forKey:[subChild name]];
                }
                else 
                {
                    [value setObject:[subChild stringValue] forKey:[subChild name]];
                }
            }
        }
        
        return value;
    }
    return nil;
}
- (id)parseWithString:(NSString *)xmlString
{
    CXMLDocument *doc = [[[CXMLDocument alloc] initWithXMLString:xmlString options:0 error:nil] autorelease];
    
    return [self parseChild:[[doc children] objectAtIndex:0]];
}

@end
