//
//  IndicatorView.m
//  lertViewController
//
//  Created by qoo hudsioo on 3/25/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import "IndicatorView.h"

@implementation IndicatorView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
        [self setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
       [self setCenter:CGPointMake(160, 200)];
        self.hidesWhenStopped = YES;
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
