//
//  FirstViewController.h
//  lertViewController
//
//  Created by qoo hudsioo on 3/11/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
@class AppDelegate;
@class IndicatorView;
@class UserEntity;
@interface locationViewController : UIViewController<UINavigationBarDelegate,UINavigationControllerDelegate,MKMapViewDelegate,CLLocationManagerDelegate>{
    
    CLLocationManager *locationManager;
    CLLocation * newco;
    float newLat;
    float newLong;
    IndicatorView * acView;
    


}
@property (nonatomic, retain) NSMutableArray * friendArray;
@property (nonatomic,retain) IBOutlet UINavigationBar * navBar;
@property(nonatomic,retain)IBOutlet MKMapView *mapView;
@property (nonatomic, retain) AppDelegate * appDelegate;
@property (nonatomic, retain) UserEntity *user;
-(id)initWithUser:(UserEntity *)_user;

@end
