//
//  AddServiceController.m
//  iLertU
//
//  Created by Chakrit Paniam on 3/11/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "AddServiceController.h"

#import "CustomAddServiceViewController.h"

#import "WebViewController.h"

#import "XMLConverter.h"
#import "CXMLDocument.h"
#import "ASIFormDataRequest.h"
#import "UserEntity.h"

#import "DetailServiceViewController.h"

@implementation AddServiceController


@synthesize arrayOfData;
@synthesize customCell;
@synthesize toolBar;
@synthesize service;
@synthesize lastPageNumber;

@synthesize readMore;
@synthesize segmentedControl;
@synthesize user;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithUser:(UserEntity *)_user{
    self = [super initWithNibName:@"AddServiceController" bundle:nil];
    if (self) {
        self.user = _user;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)insertDataFormXML:(id)xmlResult{
    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        self.arrayOfData = [xmlResult objectForKey:@"serviceCategories"];
        [self.tableView reloadData];
        
    }
    
}

#pragma mark - View lifecycle

-(void)setUpData
{
    
    NSString *pathUrl = [NSString stringWithFormat:@"%@/services/categories/list",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                        <lertParams type='ilertu.service-categories-list'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>%@</lastedUpdateDate>                        <language>%@</language>                        </common>                        <keywords/>                        </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,self.user.lastedUpdateDate,Language];
    
    
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
        XMLConverter *xml = [[XMLConverter alloc] init];
        [self insertDataFormXML:[xml parseWithString:responseString]];
        
        // NSLog(@"service cat = %@",xmlReader.result);
        /* if ([[xmlReader.result objectAtIndex:0] objectForKey:@"errorDesc"]) {
         UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlReader.result objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alertMessageView show];
         [alertMessageView release];
         }*/
        
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];

    
    
    
}
-(void)setupUI{
    self.tableView.separatorColor=[UIColor clearColor];
	self.navigationController.navigationBar.tintColor=[UIColor colorWithRed:0.3 green:0.7 blue:0.7 alpha:1.0];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    self.tableView.separatorColor=[UIColor clearColor];
    self.parentViewController.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"addin_bg.png"]];
    
    
    self.segmentedControl = [[UISegmentedControl alloc] initWithItems:
                             [NSArray arrayWithObjects:
                              [NSString stringWithString:NSLocalizedString(@"Category", @"")],
                              [NSString stringWithString:NSLocalizedString(@"List", @"")],
                              nil]];
    self.segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    // segmentedControl.tintColor = [UIColor blackColor];
    
    [self.segmentedControl setSelectedSegmentIndex:0];
    
    [self.segmentedControl addTarget:self action:@selector(push:) forControlEvents:UIControlEventValueChanged];
    self.segmentedControl.frame = CGRectMake(0, 0, 120 , 40);
    // segmentedControl.momentary = YES;
    
    UIBarButtonItem *segmentBarItem = [[UIBarButtonItem alloc] initWithCustomView:self.segmentedControl];
    self.navigationItem.rightBarButtonItem = segmentBarItem; 
}

-(void)gerServiceXML:(id)xmlResult{
    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        self.arrayOfData = [xmlResult objectForKey:@"services"];
        self.lastPageNumber = [xmlResult objectForKey:@"lastPageNumber"];
        if ([[xmlResult objectForKey:@"haveMoreItems"] isEqualToString:@"true"]) {
            self.readMore = YES;
        }
        else {
            self.readMore = NO;
        }
        [self.tableView reloadData];
    }
    
}
-(void)getAllService{
    NSString *pathUrl = [NSString stringWithFormat:@"%@/services/search",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = @"th";//[plistDict objectForKey:@"Language"];
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                        <lertParams type='ilertu.services-search'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>%@</lastedUpdateDate>                        <language>%@</language>                        </common>                        <keywords/>                        </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,self.user.lastedUpdateDate,Language];
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
        XMLConverter *xml = [[XMLConverter alloc] init];
        [self gerServiceXML:[xml parseWithString:responseString]];
        
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    

}

-(void)push:(id)sender{
    
    if ([self.segmentedControl selectedSegmentIndex] == 0) {
        NSLog(@"0");
        self.tableView.tableHeaderView = nil;
        [self.segmentedControl setSelectedSegmentIndex:0];
        [self setupUI];
        [self setUpData];
        [self.tableView reloadData];        
        
        
    }
    else{
         NSLog(@"1");
        [self.segmentedControl setSelectedSegmentIndex:1];
        
        search = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
        search.delegate = self;
        self.tableView.tableHeaderView = search;
        [search release];
        
        self.tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLineEtched;
        self.tableView.separatorColor=[UIColor blackColor];
        [self getAllService];
        [self.tableView reloadData];

       
     
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Service";
    
    [self setupUI];
    [self setUpData];
   
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        
        int count=self.arrayOfData.count%3;
        if(count==0){
            return self.arrayOfData.count/3;
        }
        else{
            return (self.arrayOfData.count/3) +1;
        }
    }
    else{
        if (self.readMore) {
            return self.arrayOfData.count+1;
        }
        else{
            return self.arrayOfData.count;
        }
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        static NSString *CellIdentifier = @"Cell";    
        CustomAddServiceViewController *cell = (CustomAddServiceViewController *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        //if (cell == nil){
        [[NSBundle mainBundle] loadNibNamed:@"CustomAddServiceViewController" owner:self options:nil];
        cell = customCell;
        //}
        int arrayCount=self.arrayOfData.count;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        cell.backgroundColor=[UIColor clearColor];  
        
        NSString *buttonText=[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+0] objectForKey:@"name"];
        
        NSURL* aURL = [NSURL URLWithString:[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+0] objectForKey:@"imageUrl"]];
        NSData* data = [[NSData alloc] initWithContentsOfURL:aURL];
        [cell.leftSideButton setBackgroundImage:[UIImage imageWithData:data] forState:UIControlStateNormal] ;
        [cell.leftSideButton setContentMode:UIViewContentModeCenter] ;
        cell.leftSideButton.backgroundColor=[UIColor clearColor]; 
        cell.leftSideButton.tag = ((indexPath.row)*3)+0;
        cell.leftSideLabel.text=buttonText;
        
        if(((indexPath.row)*3)+2<arrayCount){
            
            buttonText=[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+1] objectForKey:@"name"];
            
            NSURL* aURL = [NSURL URLWithString:[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+1] objectForKey:@"imageUrl"]];
            NSData* data = [[NSData alloc] initWithContentsOfURL:aURL];
            
            [cell.middleSideButton setBackgroundImage:[UIImage imageWithData:data] forState:UIControlStateNormal] ;
            [cell.middleSideButton setContentMode:UIViewContentModeCenter] ;
            cell.middleSideButton.backgroundColor=[UIColor clearColor]; 
            
            cell.middleSideButton.tag = ((indexPath.row)*3)+1 ;
            
            cell.middleSideLabel.text=buttonText;
            
            buttonText=[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+2] objectForKey:@"name"];
            aURL = [NSURL URLWithString:[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+2] objectForKey:@"imageUrl"]];
            data = [[NSData alloc] initWithContentsOfURL:aURL];
            
            [cell.rightSideButton setBackgroundImage:[UIImage imageWithData:data] forState:UIControlStateNormal] ;
            [cell.rightSideButton setContentMode:UIViewContentModeCenter] ;
            cell.rightSideButton.backgroundColor=[UIColor clearColor]; 
            
            cell.rightSideButton.tag = ((indexPath.row)*3)+2 ;
            //  cell.rightSideLabel.textColor = [UIColor colorWithRed:0.16 green:0.47 blue:0.02 alpha:1.0] ;
            
            cell.rightSideLabel.text=buttonText;
            
        }
        else if(((indexPath.row)*3)+1<arrayCount){
            
            buttonText=[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+1] objectForKey:@"name"];
            
            NSURL* aURL = [NSURL URLWithString:[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+1] objectForKey:@"imageUrl"]];
            NSData* data = [[NSData alloc] initWithContentsOfURL:aURL];
            
            [cell.middleSideButton setBackgroundImage:[UIImage imageWithData:data] forState:UIControlStateNormal] ;
            [cell.middleSideButton setContentMode:UIViewContentModeCenter] ;
            cell.middleSideButton.backgroundColor=[UIColor clearColor]; 
            
            cell.middleSideButton.tag = ((indexPath.row)*3)+1 ;
            
            cell.middleSideLabel.text=buttonText;
            
            cell.rightSideButton.hidden=YES;
            cell.rightSideLabel.hidden=YES;
            
        }
        else{
            cell.middleSideButton.tag = 10 ;
            cell.middleSideLabel.hidden=YES;
            cell.middleSideButton.hidden=YES;
            
            cell.rightSideButton.hidden=YES;
            cell.rightSideLabel.hidden=YES;
            
        }
        
        return cell;
        
    }
    
    else{
        
        
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell2 == nil) {
            cell2 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        }
        
        // Configure the cell...
        cell2.accessoryType = 1;
        if (indexPath.row == [self.arrayOfData count] && self.readMore) {
            cell2.textLabel.text = @"Get More";
            cell2.imageView.image = nil;
        }
        else{
            cell2.textLabel.text = [[self.arrayOfData objectAtIndex:indexPath.row] objectForKey:@"name"];
            cell2.detailTextLabel.text = [[self.arrayOfData objectAtIndex:indexPath.row] objectForKey:@"desc"];
            
            NSURL* aURL = [NSURL URLWithString:[[self.arrayOfData objectAtIndex:indexPath.row] objectForKey:@"imageUrl"]];
            NSData* data = [[NSData alloc] initWithContentsOfURL:aURL];
            
            //  NSString * imagePath = [NSURL URLWithString:[[self.myServiceArray objectAtIndex:indexPath.row] objectForKey:@"imageUrl"]];
            cell2.imageView.image = [UIImage imageWithData:data];        
        }    
        return cell2;
    }
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{   
    if(self.segmentedControl.selectedSegmentIndex == 1){
        if (self.readMore && indexPath.row == [self.arrayOfData count]) {
            if (indexPath.row == [self.arrayOfData count]) {
                NSLog(@"getmore click");
            }
        }
        else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
        NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
        
        NSString *Language;
        Language = [plistDict objectForKey:@"Language"];
        
        NSString *path = [NSString stringWithFormat:@"http://202.60.194.200/ilertu_addins/ClientWeb/0_1/Service/Add/%@?serviceId=%@&lang=%@&deviceType=ios&deviceId=%@&currentVersion=1.0a",self.user.idUser,[[self.arrayOfData objectAtIndex:indexPath.row] objectForKey:@"id"],Language,[[UIDevice currentDevice] uniqueIdentifier]];
        
        WebViewController *webView = [[WebViewController alloc] initWithURL:path];
        [self.navigationController pushViewController:webView animated:YES];
        }
    }
}

-(IBAction)addServiceButtonTapped:(id)sender{
    NSLog(@"%d",[sender tag]);
    DetailServiceViewController *detail = [[DetailServiceViewController alloc] initWithUser:self.user CatID:[[self.arrayOfData objectAtIndex:[sender tag]] objectForKey:@"id"]];
     [self.navigationController pushViewController:detail animated:YES];
   /* if ([sender tag] == 0) {
        WebViewController *webView = [[WebViewController alloc] initWithURL:@"http://www.google.com"];
        [self.navigationController pushViewController:webView animated:YES];
    }
    else {
        WebViewController *webView = [[WebViewController alloc] initWithURL:@"http://www.apple.com"];
        [self.navigationController pushViewController:webView animated:YES];
    }*/
    
}

#pragma mark UISearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // only show the status bar’s cancel button while in edit mode
    search.showsCancelButton = YES;
    search.autocorrectionType = UITextAutocorrectionTypeNo;
    // flush the previous search content
    //[tableData removeAllObjects];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    search.showsCancelButton = NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    // if a valid search was entered but the user wanted to cancel, bring back the main list content
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    [self getAllService];
}
// called when Search (in our case “Done”) button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"SearchButtonClicked");
    NSString *pathUrl = [NSString stringWithFormat:@"%@/services/search",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                        <lertParams type='ilertu.services-search'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>%@</lastedUpdateDate>                        <language>%@</language>                        </common>                        <keywords>                        <categoryId>1</categoryId>                        <serviceKeyword>%@</serviceKeyword>                        <lastPageNumber>%@</lastPageNumber>                        </keywords>                        </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,self.user.lastedUpdateDate,Language,searchBar.text,self.lastPageNumber];
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
        XMLConverter *xml = [[XMLConverter alloc] init];
        [self gerServiceXML:[xml parseWithString:responseString]];
        
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
        
    // The user clicked the [X] button or otherwise cleared the text.
    if([searchText length] == 0) {
        [self getAllService];
    }
}

@end
