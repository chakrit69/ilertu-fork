//
//  AppDelegate.h
//  iLertU
//
//  Created by Chakrit Paniam on 2/28/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "FBConnect.h"


@class LogInViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>{
    
}

@property (strong, nonatomic) UINavigationController *navigationController;
@property (nonatomic, retain) CLLocationManager *location;
@property (nonatomic, retain) CLLocation *locate;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) LogInViewController *viewController;

@property (nonatomic, retain) Facebook *facebook;

@property (nonatomic, retain) NSMutableDictionary *userPermissions;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)shareCurrentLocation;

@end
