//
//  addNewFriendViewController.m
//  iLertU
//
//  Created by qoo hudsioo on 4/10/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import "addNewFriendViewController.h"
#import "ASIFormDataRequest.h"
#import "XMLConverter.h"
#import "UserEntity.h"

@implementation addNewFriendViewController
@synthesize emailF;
@synthesize phoneF;
@synthesize user;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithUser:(UserEntity *)_user{
    self = [super initWithNibName:@"addNewFriendViewController" bundle:nil];
    if (self) {
        self.user = _user;
    }
    return self;
}

-(void)insertDataFormXML:(id)xmlResult{
    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        NSLog(@"add friend result = %@",xmlResult);
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}



-(void)requestFriend{
    
    NSString *pathUrl = [NSString stringWithFormat:@"%@/user/friends/add",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                        <lertParams type='ilertu.user.friends-add'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>0</lastedUpdateDate>                        <language>%@</language>                        </common>                        <newRequest>                        <email>%@</email>                        <phone>%@</phone>                        </newRequest></lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,Language,self.emailF.text,self.phoneF.text];
    
    
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
        NSLog(@"add friend = %@",responseString);
        XMLConverter *xml = [[XMLConverter alloc] init];
        [self insertDataFormXML:[xml parseWithString:responseString]];
        //  [inDicView stopAnimating];
        
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    
}

- (IBAction)sendRequest:(id)sender{
    [self requestFriend];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"new Request";
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
