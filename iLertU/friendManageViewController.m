//
//  friendManageViewController.m
//  iLertU
//
//  Created by qoo hudsioo on 4/1/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import "friendManageViewController.h"
#import "UserEntity.h"
#import "XMLReader.h"
#import "ASIFormDataRequest.h"
#import "addNewFriendViewController.h"
#import "XMLConverter.h"

@implementation friendManageViewController

@synthesize user;
@synthesize friendArray;
@synthesize headerView;
@synthesize switchAll;
@synthesize showName;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithUser:(UserEntity *)_user{
    self = [super initWithStyle:UITableViewStylePlain];
    self = [super initWithNibName:@"friendManageViewController" bundle:nil];
    if (self) {
        self.user = _user;
    }
    return self;
}


-(void)insertDataFormXML:(id)xmlResult{
    
    self.friendArray = [[NSMutableArray alloc] init];
    fr = [[NSDictionary alloc]init];
    frIndex = [[NSMutableArray alloc] init];
    frName = [[NSMutableArray alloc]init];

    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        NSLog(@"xmlResult = %@",xmlResult);
        // เผื่อเอา array มาทำอะไร
        
        // Save the context.
        self.showName = [[NSArray alloc]init];
        self.friendArray = [xmlResult objectForKey:@"friends"];
        if (self.friendArray != NULL) {
          
            for (NSDictionary * i in self.friendArray){
                char alpha = [[i objectForKey:@"name"] characterAtIndex:0];
                //  char alphabet = [[listOfStates objectAtIndex:i] characterAtIndex:0];
                NSString *uniChar = [NSString stringWithFormat:@"%C", alpha];
                
                if (![frIndex containsObject:uniChar])
                {
                    [frIndex addObject:uniChar];
                }
            }
            [frIndex sortUsingSelector:@selector(compare:)];
            for (NSDictionary * i in self.friendArray){
                [frName addObject:[i objectForKey:@"name"]];
                
            }
            [frName sortUsingSelector:@selector(compare:)];
        }
        
        
        NSLog(@"requestArray = %@",[[xmlResult objectForKey:@"friends"] class]);
        [self.tableView reloadData];
       [self tableView:self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    
}


-(void)setupData{
    
    NSString *pathUrl = [NSString stringWithFormat:@"%@/user/friends/list",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                        <lertParams type='ilertu.user.friends-list'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>0</lastedUpdateDate>                        <language>%@</language>                        </common>                        </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,Language];
    
    
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
        XMLConverter *xml = [[XMLConverter alloc] init];
        
        
        NSLog(@"request responseStringn = %@",responseString);
        
        
        
        [self insertDataFormXML:[xml parseWithString:responseString]];
        //  [inDicView stopAnimating];
       // [self.navigationController popViewControllerAnimated:YES];    
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    
}
-(void)insertRequestFormXML:(id)xmlResult{
    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        NSLog(@"xmlResult = %@",xmlResult);

    }
    
}

- (void)updatePermittion:(NSString*)idToUpdate status:(NSString*)sString{
    
    NSString *pathUrl = [NSString stringWithFormat:@"%@/user/friends/permission",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                        <lertParams type='ilertu.user.friends-permission'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>0</lastedUpdateDate>                        <language>%@</language>                        </common>   <friend id='%@'>                        <allow>%@</allow>                        </friend>                     </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,Language,idToUpdate,sString];
    
    
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
        XMLConverter *xml = [[XMLConverter alloc] init];
        
        
        NSLog(@"request responseStringn = %@",responseString);
        
        
        
        [self insertRequestFormXML:[xml parseWithString:responseString]];
        //  [inDicView stopAnimating];
        // [self.navigationController popViewControllerAnimated:YES];    
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.friendArray = [[NSMutableArray alloc]init];
    self.title = @"friends";
    [self setupData];
    
    UIBarButtonItem *rbarButtonItem=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addFriend)];    
                          
    
    self.navigationItem.rightBarButtonItem = rbarButtonItem;
    
    fr = [[NSDictionary alloc]init];

    
    
    frIndex = [[NSMutableArray alloc] init];
    
    
    frName = [[NSMutableArray alloc]init];
    
    
    [self.tableView setTableHeaderView:self.headerView];
   
    
}
- (void)addFriend{
    addNewFriendViewController * addNewFrVC = [[addNewFriendViewController alloc]initWithUser:self.user];
    [self.navigationController pushViewController:addNewFrVC animated:YES];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
        
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.friendArray == NULL) {
        return 0;
    }else {
         return [frIndex count];
    }
   
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.friendArray == NULL) {
        return 0;
    }else {
        NSString * alpha = [frIndex objectAtIndex:section];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c]%@",alpha];
        NSArray *frx = [frName filteredArrayUsingPredicate:predicate];
        return [frx count];
   }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                       reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryNone;
         cell.accessoryView = tbSwitch;
        
    }
  
    if (self.friendArray == NULL) {
        NSLog(@"NULL");
    }else {
        NSString * alpha = [frIndex objectAtIndex:[indexPath section]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c]%@", alpha];
        self.showName = [frName filteredArrayUsingPredicate:predicate];
    
    
        if ([self.showName count] >0) {
           tbSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(0, 0, 40, 20)];
            cell.accessoryView  = tbSwitch;
           // [tbSwitch setOn:YES animated:YES];
            
            NSDictionary * tmp = [[NSDictionary alloc]initWithDictionary:[self findFriend:[self.showName objectAtIndex:indexPath.row]]];
            if ([[tmp objectForKey:@"allow"] isEqualToString:@"true"]) {
                [tbSwitch setOn:YES animated:YES];
            }else {
                [tbSwitch setOn:NO animated:YES];
            }
            
//            if (self.switchAll.on) {
//                [tbSwitch setOn:YES animated:YES];
//            }else {
//                [tbSwitch setOn:NO animated:YES];
//            }
            
            [tbSwitch addTarget:self action:@selector(switchChanged:)  forControlEvents:UIControlEventValueChanged];
            
            NSString * cellText = [self.showName objectAtIndex:indexPath.row];
            cell.textLabel.text = cellText;
            
        }
    }
  
    return cell;
}

- (NSDictionary*)findFriend:(NSString*)name{
    NSDictionary * tmp = [[NSDictionary alloc]init];
    for (NSDictionary * i in self.friendArray){
        if ([name isEqualToString: [i objectForKey:@"name"]]) {
            tmp = [i mutableCopy];
        }
    }
    
    return tmp;
}

- (IBAction)switchAllDidChange:(id)sender{
    
    if (self.switchAll.on) {
        statusAll = YES;
    }else {
        statusAll = NO;
    }
    
}

- (void) switchChanged:(id)sender{
    UISwitch* switchControl = sender;
  //  NSLog( @"The switch is %@", switchControl.on ? @"ON" : @"OFF" );
    UITableViewCell *cell = (UITableViewCell *)switchControl.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];  
    NSLog(@"Indexpath = %d",indexPath.row);
    
    NSLog(@"name %@",[self findFriend:[frName objectAtIndex:indexPath.row]]);
    NSDictionary * tmp = [[NSDictionary alloc]initWithDictionary:[self findFriend:[frName objectAtIndex:indexPath.row]]];
    [self updatePermittion:[tmp objectForKey:@"id"] status:switchControl.on ? @"true" : @"false"];

}

- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section {
    if (self.friendArray == NULL) {
        return NULL;
    }else {
        return [frIndex objectAtIndex:section];
    }
    
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (self.friendArray == NULL) {
        return NULL;
    }else {
        return frIndex;
    }
}



- (void)updateDelete:(NSString*)idToUpdate{
    
    NSString *pathUrl = [NSString stringWithFormat:@"%@/user/friends/delete",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                        <lertParams type='ilertu.user.friends-delete'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen>                        <username>%@</username>                        <password>%@</password>                        </authen>                        <lastedUpdateDate>0</lastedUpdateDate>                        <language>%@</language>                        </common>   <friend id='%@'>                                               </friend>                     </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,Language,idToUpdate];
    
    
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
    //    XMLConverter *xml = [[XMLConverter alloc] init];
        
        
        NSLog(@"request responseStringn = %@",responseString);
        [self setupData];
        
        
       //[self insertDataFormXML:[xml parseWithString:responseString]];
        //  [inDicView stopAnimating];
        // [self.navigationController popViewControllerAnimated:YES];    
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView == alertViewDelete) {
        if (buttonIndex == 0) {
            
        } else if(buttonIndex == 1){
            [self updateDelete:[fr objectForKey:@"id"]];
          //  [self setupData];
           // [self.tableView reloadData];
        }
    }

    
	
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0) {
        alertViewDelete = [[UIAlertView alloc] initWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Do you want to Delete '%@'",[fr objectForKey:@"name"]] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alertViewDelete show];
        [alertViewDelete release];
        
        
    }else if(buttonIndex == 1){
        alertViewDelete = [[UIAlertView alloc] initWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Do you want to Delete '%@'",[fr objectForKey:@"name"]] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alertViewDelete show];
        [alertViewDelete release];
    }
}

-(void)toggleDelete:(NSDictionary*)frTodel{
    UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Actons for %@",[frTodel objectForKey:@"name"]]
													  delegate:self
											 cancelButtonTitle:@"Cancel"
										destructiveButtonTitle:NULL
											 otherButtonTitles:@"Change groups" ,@"Delete",nil];
	
    
	[menu showInView:self.view];
	//[menu setBounds:CGRectMake(0,0,320, 260)];
	[menu release];
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * alpha = [frIndex objectAtIndex:[indexPath section]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c]%@", alpha];
    NSArray *frx = [frName filteredArrayUsingPredicate:predicate];
    int count=0;
    for (NSDictionary *  i in self.friendArray ){
        if ([[frx objectAtIndex:indexPath.row] isEqualToString:[i objectForKey:@"name"]]) {
            [self toggleDelete:i];
            fr = [self.friendArray objectAtIndex:count];
            indexF = count;
            
        }
        count++;
    }
}

@end
