//
//  LogInViewController.m
//  iLertU
//
//  Created by Chakrit Paniam on 2/28/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "LogInViewController.h"
#import "AddInViewController.h"
#import "SignUpViewController.h"
#import "SignInViewController.h"
#import "ASIFormDataRequest.h"
#import "XMLReader.h"
#import "NSDataAdditions.h"
#import "UserEntity.h"
#import "IndicatorView.h"

@implementation LogInViewController


@synthesize loginWithFBButton;
@synthesize signInButton;
@synthesize signUpButton;
@synthesize skipButton;

@synthesize activityIndicator;

@synthesize permissions;

@synthesize plistDict;

@synthesize appDelegate;

@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;

-(void)insertDataFormXML:(NSArray *)xmlArray{
    
    if ([[xmlArray objectAtIndex:0] objectForKey:@"errorDesc"]) {
        //NSLog(@"ERROR");
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlArray objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
        // NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
        
        UserEntity *user = (UserEntity *)[NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
        user.idUser =[[xmlArray objectAtIndex:0] objectForKey:@"id"];
        user.name = [[xmlArray objectAtIndex:0] objectForKey:@"given"];
        user.salutation = [[xmlArray objectAtIndex:0] objectForKey:@"salutation"];
        user.active = [[xmlArray objectAtIndex:0] objectForKey:@"active"];
        user.surname = [[xmlArray objectAtIndex:0] objectForKey:@"family"];
        user.idCard = [[xmlArray objectAtIndex:0] objectForKey:@"idCard"];
        user.gender = [[xmlArray objectAtIndex:0] objectForKey:@"gender"];
        user.birthday = [[xmlArray objectAtIndex:0] objectForKey:@"birthday"];
        user.imageUrl = [[xmlArray objectAtIndex:0] objectForKey:@"imageUrl"];
        user.phone = [[xmlArray objectAtIndex:0] objectForKey:@"phone"];
        user.username = [[xmlArray objectAtIndex:0] objectForKey:@"username"];
        user.password = [[xmlArray objectAtIndex:0] objectForKey:@"password"];
        user.lastedUpdateDate =[[xmlArray objectAtIndex:1] objectForKey:@"lastedUpdateDate"];
        
        
        // If appropriate, configure the new managed object.
        // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.
        //        [newManagedObject setValue:[[xmlArray objectAtIndex:0] objectForKey:@"given"] forKey:@"name"];
        //        [newManagedObject setValue:[[xmlArray objectAtIndex:0] objectForKey:@"active"] forKey:@"active"];
        //        [newManagedObject setValue:[[xmlArray objectAtIndex:0] objectForKey:@"idCard"] forKey:@"idCard"];
        //        [newManagedObject setValue:[[xmlArray objectAtIndex:0] objectForKey:@"salutation"] forKey:@"salutation"];
        //        [newManagedObject setValue:[[xmlArray objectAtIndex:0] objectForKey:@"family"] forKey:@"surname"];
        //        [newManagedObject setValue:[[xmlArray objectAtIndex:0] objectForKey:@"gender"] forKey:@"gender"];
        //        [newManagedObject setValue:[[xmlArray objectAtIndex:0] objectForKey:@"phone"] forKey:@"phone"];
        //        [newManagedObject setValue:[[xmlArray objectAtIndex:0] objectForKey:@"birthday"] forKey:@"birthday"];
        //        [newManagedObject setValue:[[xmlArray objectAtIndex:0] objectForKey:@"imageUrl"] forKey:@"imageUrl"];
        //        [newManagedObject setValue:[[xmlArray objectAtIndex:0] objectForKey:@"username"] forKey:@"username"];
        //        [newManagedObject setValue:[[xmlArray objectAtIndex:0] objectForKey:@"password"] forKey:@"password"];
        
        // Save the context.
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        
        AddInViewController *addInView = [[AddInViewController alloc] initWithUser:user];
        UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:addInView] autorelease];
        [self.navigationController presentModalViewController:navController animated:YES];
        [addInView release];
        
        
    }
    
}


- (void)showActivityIndicator {
    
    if (![activityIndicator isAnimating]) {
         self.view.userInteractionEnabled = NO;
      //  self.view
        [activityIndicator startAnimating];
    }
}
- (void)hideActivityIndicator {
    if ([activityIndicator isAnimating]) {
        [activityIndicator stopAnimating];
        self.view.userInteractionEnabled = YES;
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - Facebook API Calls

- (void)fbDidLogin {
    

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[appDelegate.facebook accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[appDelegate.facebook expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    
}

/**
 * Make a Graph API Call to get information about the current logged in user.
 */








-(void)setupView{
    
    
    self.title = @"iLertU 1.0";
    
    self.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"bg_iphone.png"]];
    [self.loginWithFBButton setImage:[UIImage imageNamed:@"button_fb.png"] forState:UIControlStateNormal];
    [self.signInButton setImage:[UIImage imageNamed:@"button_signin.png"] forState:UIControlStateNormal];
    [self.signUpButton setImage:[UIImage imageNamed:@"button_signup.png"] forState:UIControlStateNormal];
    
    
    [self.skipButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [self.skipButton setTitleShadowColor:[UIColor clearColor] forState:UIControlStateNormal];
    [self.skipButton setTitleColor:[UIColor clearColor] forState:UIControlStateHighlighted];
    [self.skipButton setTitleShadowColor:[UIColor clearColor] forState:UIControlStateHighlighted];
    [self.skipButton setImage:[UIImage imageNamed:@"button_skip.png"] forState:UIControlStateNormal];
    

  
    
    // Activity Indicator
    int xPosition = (self.view.bounds.size.width / 2.0) - 15.0;
    int yPosition = (self.view.bounds.size.height / 2.0) - 15.0;

    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(xPosition, yPosition, 30, 30)];
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [self.view addSubview:activityIndicator];
    

}

-(void)setupFacebook{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"] 
        && [defaults objectForKey:@"FBExpirationDateKey"]) {
        appDelegate.facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
        appDelegate.facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
    }
    if (![appDelegate.facebook isSessionValid]) {
        [appDelegate.facebook authorize:nil];
    }

    if (!kAppId) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Setup Error"
                                  message:@"Missing app ID. You cannot run the app until you provide this in the code."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil,
                                  nil];
        [alertView show];
        [alertView release];
    } else {
        // Now check that the URL scheme fb[app_id]://authorize is in the .plist and can
        // be opened, doing a simple check without local app id factored in here
        NSString *url = [NSString stringWithFormat:@"fb%@://authorize",kAppId];
        BOOL bSchemeInPlist = NO; // find out if the sceme is in the plist file.
        NSArray* aBundleURLTypes = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleURLTypes"];
        if ([aBundleURLTypes isKindOfClass:[NSArray class]] &&
            ([aBundleURLTypes count] > 0)) {
            NSDictionary* aBundleURLTypes0 = [aBundleURLTypes objectAtIndex:0];
            if ([aBundleURLTypes0 isKindOfClass:[NSDictionary class]]) {
                NSArray* aBundleURLSchemes = [aBundleURLTypes0 objectForKey:@"CFBundleURLSchemes"];
                if ([aBundleURLSchemes isKindOfClass:[NSArray class]] &&
                    ([aBundleURLSchemes count] > 0)) {
                    NSString *scheme = [aBundleURLSchemes objectAtIndex:0];
                    if ([scheme isKindOfClass:[NSString class]] &&
                        [url hasPrefix:scheme]) {
                        bSchemeInPlist = YES;
                    }
                }
            }
        }
        // Check if the authorization callback will work
        BOOL bCanOpenUrl = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString: url]];
        if (!bSchemeInPlist || !bCanOpenUrl) {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Setup Error"
                                      message:@"Invalid or missing URL scheme. You cannot run the app until you set up a valid URL scheme in your .plist."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil,
                                      nil];
            [alertView show];
            [alertView release];
        }
    }


}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    
    inDicView = [[IndicatorView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];  
    [self.view addSubview:inDicView];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
       
    self.permissions = [[NSArray alloc] initWithObjects:
                            @"user_birthday", 
                            @"read_stream",
                            nil];
   

   
    if (__managedObjectContext == nil) 
    { 
        __managedObjectContext = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext]; 
    }

    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // Hide the activitiy indicator
    [self hideActivityIndicator];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(IBAction)loginWithFBButtonTapped:(id)sender{
    
    [self setupFacebook];
    [appDelegate.facebook authorize:self.permissions];
    [appDelegate.facebook requestWithGraphPath:@"me" andDelegate:self];
           
    
}
-(IBAction)signIn:(id)sender{
    SignInViewController *signIn = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
    [self.navigationController pushViewController:signIn animated:YES];
    [signIn release];
}
-(IBAction)signUP:(id)sender{
    SignUpViewController *signUp = [[SignUpViewController alloc] initWithNibName:@"SignUpViewController" bundle:nil];
    [self.navigationController pushViewController:signUp animated:YES];
    [signUp release];
}
-(IBAction)skipButtonTapped:(id)sender{
    
    //[self showActivityIndicator];
    NSMutableArray *arrayOfSampleData=[NSMutableArray arrayWithObjects:@"Arunsawad",@"Doo D",@"Enmergency Call",@"iService",@"Lert",nil];
    AddInViewController *addInViewController = [[AddInViewController alloc] initWithAddIn:arrayOfSampleData];
    
    
    UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:addInViewController] autorelease];
    [self presentModalViewController:navController animated:YES];
    [addInViewController release];
     
}


#pragma mark - FBRequestDelegate Methods
/**
 * Called when the Facebook API request has returned a response. This callback
 * gives you access to the raw response. It's called before
 * (void)request:(FBRequest *)request didLoad:(id)result,
 * which is passed the parsed response object.
 */
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    
    //NSLog(@"%@", response);
    
}

/**
 * Called when a request returns and its response has been parsed into
 * an object. The resulting object may be a dictionary, an array, a string,
 * or a number, depending on the format of the API response. If you need access
 * to the raw response, use:
 *
 * (void)request:(FBRequest *)request
 *      didReceiveResponse:(NSURLResponse *)response
 */

- (void)request:(FBRequest*)request didLoadRawResponse:(NSData*)data
{
    NSString *response = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSLog(@"%@", response);
    [response release];
}

- (void)request:(FBRequest *)request didLoad:(id)result {
    if ([result isKindOfClass:[NSDictionary class]])
    {
        [inDicView startAnimating];
        
        NSString *satulation;
        if ([[result objectForKey: @"gender"] isEqual:@"male"]) {
            satulation = @"Mr.";
                    }
        else {
            satulation = @"Miss";
        }
       
        NSLog(@"birthday = %@",[result objectForKey:@"birthday"]);
        
        NSString *fbuid = [result objectForKey:@"id"];
        NSURL *urlPic = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?", fbuid]];
        NSData *data = [NSData dataWithContentsOfURL:urlPic];
        UIImage *profileImage = [UIImage imageWithData:data];
        NSData *imageData = UIImageJPEGRepresentation(profileImage, 1.0);
        NSString *imageBase64String = [imageData base64Encoding];

       
        NSString *pathUrl = [NSString stringWithFormat:@"%@/user/sign-up/facebook",ILERTU];
        NSURL *url = [NSURL URLWithString:pathUrl];
        
        
        ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
        
        [request2 addRequestHeader:@"content-type" value:@"text/xml"];
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
        NSMutableDictionary* plist = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
        
        NSString *Language;
        Language = [plist objectForKey:@"Language"];
        
        
        NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                            <lertParams type='ilertu.user.sign-up'>                            <common>                            <device>                            <deviceType>ios</deviceType>                            <deviceId>%@</deviceId>                            <currentVersion>1.0a</currentVersion>                            </device>                            <authen/>                            <lastedUpdateDate/>                            <language>%@</language>                            </common>                            <user>                            <idCard>%@</idCard>                            <salutation>%@</salutation>                            <name>                            <given>%@</given>                            <family>%@</family>                            </name>                            <phone>%@</phone>                            <birthday>%@</birthday>                            <authen>                            <username>%@</username>                            <password>%@</password>                            </authen>                            <image>                            <imageSource>%@</imageSource>                            <postType>1</postType>                            </image>                            </user>                            </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],Language,[result objectForKey: @"id"],satulation,[result objectForKey: @"first_name"],[result objectForKey: @"last_name"],[result objectForKey: @"phone"],[result objectForKey:@"birthday"],[result objectForKey:@"username"],[result objectForKey:@"id"],imageBase64String];
        
        
        
        [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [request2 setCompletionBlock:^{
            NSString *responseString = [request2 responseString];
            
            XMLReader* xmlReader = [XMLReader xmlWithXMLString:responseString andNode:@"//user"];
            [xmlReader parseXML];
            
            
            xmlReader.XMLString = [request2 responseString];
            NSLog(@"xxxx = %@",xmlReader.result); 
            [self insertDataFormXML:xmlReader.result];
            [inDicView stopAnimating];
            
        }];
        
        [request2 setFailedBlock:^{
            NSError *error = [request2 error];
            NSLog(@"Error: %@", error.localizedDescription);
            
            UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertMessageView show];
            [alertMessageView release];
            
        }];
        
        [request2 startAsynchronous];
    }
    else {
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:@"Please insert information" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    

        
        [self hideActivityIndicator];
        
       
        
        
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (__fetchedResultsController != nil) {
        return __fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserEntity" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"name" ascending:NO] autorelease];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"] autorelease];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return __fetchedResultsController;
}   


/**
 * Called when an error prevents the Facebook API request from completing
 * successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"Err message: %@", [[error userInfo] objectForKey:@"error_msg"]);
    NSLog(@"Err code: %d", [error code]);
}





@end
