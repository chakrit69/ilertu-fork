//
//  freindRequestViewController.h
//  lertViewController
//
//  Created by qoo hudsioo on 3/14/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDelegate;
@class UserEntity;
//@class friendModel;

@interface freindRequestViewController : UITableViewController <UIActionSheetDelegate,UIAlertViewDelegate>{
    
    NSDictionary * fr;
    NSInteger indexF;
    UIAlertView* alertViewAccept;
    UIAlertView* alertViewReject;
    
    
    NSMutableArray * frIndex;
    NSMutableArray * frRequest;
    NSMutableArray * frName;

    
}
@property (nonatomic, retain) NSArray * showName;
@property (nonatomic, retain) NSMutableArray * frRequest;
@property (nonatomic, retain) AppDelegate * appDelegate;
@property (nonatomic, retain) UserEntity* user;


- (id)initWithArray:(NSMutableArray*)reFriend andUser:(UserEntity *)_user;
@end
