//
//  SecondViewController.m
//  lertViewController
//
//  Created by qoo hudsioo on 3/11/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import "lertsViewController.h"
#import "UserEntity.h"
#import "XMLConverter.h"
#import "ASIFormDataRequest.h"


@implementation lertsViewController
@synthesize user;
@synthesize iLertArray,uLertArray;
@synthesize headerView;
@synthesize secmentUI;


-(id)initWithUser:(UserEntity *)_user{
    self = [super initWithNibName:@"lertsViewController" bundle:nil];
    if (self) {
        self.user = _user;
        self.title = NSLocalizedString(@"lerts", @"lerts");
        self.tabBarItem.image = [UIImage imageNamed:@"124-bullhorn.png"];
    }
    return self;
}
	
-(IBAction)secmentDidChange:(id)sender{
    if (self.secmentUI.selectedSegmentIndex == 0) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }else if(self.secmentUI.selectedSegmentIndex ==1){
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    
}
- (void)backToAddIn{
    NSLog(@"Back");
    [self.navigationController dismissModalViewControllerAnimated:YES];
}
- (void)add{

}
- (void)viewDidLoad
{
    [super viewDidLoad];
	UIBarButtonItem *lbarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Add In" style:    
                                     UIBarButtonSystemItemUndo target:self 
                                                                   action:@selector(backToAddIn)];
    self.navigationItem.leftBarButtonItem = lbarButtonItem;
    
    UIBarButtonItem *rbarButtonItem=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add)];    
    
    
    self.navigationItem.rightBarButtonItem = rbarButtonItem;
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    [self.tableView setTableHeaderView:self.headerView];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                       reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}


@end
