//
//  SecondViewController.h
//  lertViewController
//
//  Created by qoo hudsioo on 3/11/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UserEntity;
@interface lertsViewController : UITableViewController{
    

    
}


@property (nonatomic, retain) UserEntity *user;
@property (nonatomic, retain) IBOutlet UIView * headerView;
@property (nonatomic, retain) IBOutlet UISegmentedControl * secmentUI;
@property (nonatomic, retain) NSMutableArray *uLertArray;
@property (nonatomic, retain) NSMutableArray *iLertArray;

-(id)initWithUser:(UserEntity *)_user;
-(IBAction)secmentDidChange:(id)sender;
@end
