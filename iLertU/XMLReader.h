//
//  XMLReader.h
//  TESTTouchXML
//
//  Created by Chakrit Paniam on 2/27/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMLReader : NSObject{
    
}
@property (nonatomic, retain)NSString* XMLString;
@property (nonatomic, retain)NSString* urlName;
@property (nonatomic, retain)NSString* nodeName;
@property (nonatomic, retain)NSArray* result;

-(id)initWithURL:(NSString *)url andNode:(NSString *)node;
+(id)xmlWithURL:(NSString *)url andNode:(NSString *)node;


-(id)initWithXMLString:(NSString *)xml andNode:(NSString *)node;
+(id)xmlWithXMLString:(NSString *)xml andNode:(NSString *)node;

-(void)parseXML;


@end
