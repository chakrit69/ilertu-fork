//
//  AddServiceController.h
//  iLertU
//
//  Created by Chakrit Paniam on 3/11/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CustomAddServiceViewController.h"




@class UserEntity;
@class Service;

@interface AddServiceController : UITableViewController <UISearchBarDelegate>{

    UISearchBar *search;
    
}


@property(nonatomic, retain)NSArray *arrayOfData;
@property(nonatomic, retain)IBOutlet CustomAddServiceViewController *customCell;
@property(nonatomic, retain)UIToolbar* toolBar;

@property(nonatomic, retain)UISegmentedControl *segmentedControl; 

@property(nonatomic, assign)BOOL readMore;
@property(nonatomic, retain)NSString *lastPageNumber;

@property(nonatomic, retain)UserEntity *user;
@property(nonatomic, retain)Service *service;


-(id)initWithUser:(UserEntity *)_user;
-(IBAction)addServiceButtonTapped:(id)sender;

@end
