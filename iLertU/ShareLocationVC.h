//
//  ShareLocationVC.h
//  iLertU
//
//  Created by Katanyoo Ubalee on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareLocationVC : UITableViewController
{
    IBOutlet UIView *shareZone;
    IBOutlet UIView *watchZone;
    
    IBOutlet UISwitch *shareSwitch;
    IBOutlet UISwitch *watchSwitch;
    
    IBOutlet UISlider *timeSlider;
    
    IBOutlet UILabel *timeLabel;
}

- (IBAction)shareSwitchDidChange:(id)sender;
- (IBAction)watchSwitchDidChange:(id)sender;
- (IBAction)updateTimeToShareLocation:(UISlider *)sender;
@end
