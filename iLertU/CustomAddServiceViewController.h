//
//  CustomAddServiceViewController.h
//  iLertU
//
//  Created by Chakrit Paniam on 3/11/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAddServiceViewController : UITableViewCell{
IBOutlet UIButton *mLeftSideButton;
IBOutlet UIButton *mMiddleSideButton;

IBOutlet UIButton *mRightSideButton;
IBOutlet UILabel *mLeftSideLabel;
IBOutlet UILabel *mMiddleSideLabel;

IBOutlet UILabel *mRightSideLabel;

}
@property (retain)UIButton *leftSideButton;
@property (retain)UIButton *middleSideButton;
@property (retain)UIButton *rightSideButton;
@property (retain)UILabel *leftSideLabel;
@property (retain)UILabel *middleSideLabel;
@property (retain)UILabel *rightSideLabel;

@end
