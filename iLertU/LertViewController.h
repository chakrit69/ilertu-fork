//
//  LertViewController.h
//  iLertU
//
//  Created by Chakrit Paniam on 3/12/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


@interface LertViewController : UITableViewController <CLLocationManagerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate> {
 
    CLLocationManager *locationManager;
    
}

- (IBAction)toggleView:(id)sender;

@property (nonatomic, retain) UIImagePickerController * imagePicker;

@property (nonatomic , retain)IBOutlet UIView *header;
@property (nonatomic , retain)IBOutlet UILabel *latlongLabel;
@property (nonatomic , retain)IBOutlet UITextView *lertText;
@property (nonatomic , retain)IBOutlet UIButton *takePhoto;


@end
