//
//  DetailServiceViewController.h
//  iLertU
//
//  Created by Chakrit Paniam on 4/11/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserEntity;

@interface DetailServiceViewController : UITableViewController


@property (nonatomic , retain)UserEntity *user;


@property (nonatomic, retain)NSMutableArray *serviceArray;

-(id)initWithUser:(UserEntity *)_user CatID:(NSString *)_catid;

@end
