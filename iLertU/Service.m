//
//  Service.m
//  iLertU
//
//  Created by Chakrit Paniam on 4/6/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Service.h"


@implementation Service

@dynamic active;
@dynamic name;
@dynamic desc;
@dynamic phone;
@dynamic imageUrl;
@dynamic id;
@dynamic responseStatus;

@end
