//
//  moniterViewController.m
//  lertViewController
//
//  Created by qoo hudsioo on 3/12/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import "filterViewController.h"
#import "AppDelegate.h"
//#import "friendModel.h"
#import "ASIFormDataRequest.h"
#import "UserEntity.h"

@implementation filterViewController
@synthesize headerView;
@synthesize appDelegate;
//@synthesize frTrack;
@synthesize currentSeg;
@synthesize friendArray;
@synthesize user;

-(AppDelegate*)appDelegate{
	return (AppDelegate*)[[UIApplication sharedApplication]delegate];
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    self = [super initWithNibName:@"filterViewController" bundle:nil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithUser:(UserEntity *)_user andFriendArray:(NSMutableArray*)fr_ary{
    self = [super initWithNibName:@"filterViewController" bundle:nil];
     self.friendArray = [[NSMutableArray alloc]init];
    if (self) {
        self.user = _user;
        self.friendArray = [fr_ary mutableCopy];
    }
    return self;
}

- (void)Done{
  //  [self.appDelegate trackThisPerson:frTrack];
    [self.navigationController popViewControllerAnimated:YES];
     NSLog(@"Who??? %@",[fr objectForKey:@"name"]);
    
}

-(IBAction) switchControlIndexChanged{
    if (self.currentSeg.selectedSegmentIndex == 0) {
        
       // self.appDelegate.currectStatus = YES;
        NSLog(@"YES");
    }
    else if(self.currentSeg.selectedSegmentIndex == 1){
       // self.appDelegate.currectStatus = NO;
        NSLog(@"NO");
    }
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"monitor filter";
    UIBarButtonItem *barButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:    
                                    UIBarButtonItemStylePlain target:self 
                                                                  action:@selector(Done)];
    
    self.navigationItem.rightBarButtonItem = barButtonItem; 
    [self.tableView setTableHeaderView:self.headerView];
    
    indexCheckMark = -1;
    
    fr = [[NSDictionary alloc]init];
    
    frName = [[NSMutableArray alloc]init];
    
    frIndex = [[NSMutableArray alloc] init];
   
    
    for (NSDictionary * i in self.friendArray){
        char alpha = [[i objectForKey:@"name"] characterAtIndex:0];
        //  char alphabet = [[listOfStates objectAtIndex:i] characterAtIndex:0];
        NSString *uniChar = [NSString stringWithFormat:@"%C", alpha];
        
        if (![frIndex containsObject:uniChar])
        {
            [frIndex addObject:uniChar];
        }
    }
    [frIndex sortUsingSelector:@selector(compare:)];
    for (NSDictionary * i in self.friendArray){
        [frName addObject:[i objectForKey:@"name"]];
        
    }
    [frName sortUsingSelector:@selector(compare:)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  //   [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
    
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [self.headerView release];
}



#pragma mark - Table view data source



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.friendArray == NULL) {
        return 0;
    }else {
        return [frIndex count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.friendArray == NULL) {
        return 0;
    }else {
        NSString * alpha = [frIndex objectAtIndex:section];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c]%@",alpha];
        NSArray *frx = [frName filteredArrayUsingPredicate:predicate];
        return [frx count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                       reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    NSString * alpha = [frIndex objectAtIndex:[indexPath section]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c]%@", alpha];
    NSArray * frx = [frName filteredArrayUsingPredicate:predicate];

    
    
    if ([frx count] >0) {
//        if ([[frx objectAtIndex:indexPath.row] isSelected]) {
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        } else {
//            cell.accessoryType = UITableViewCellAccessoryNone;
//        }
        NSString * cellText = [frx objectAtIndex:indexPath.row];
        cell.textLabel.text = cellText;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section {
	return [frIndex objectAtIndex:section];
    
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return frIndex;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString * alpha = [frIndex objectAtIndex:[indexPath section]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c]%@", alpha];
    NSArray *frx = [frName filteredArrayUsingPredicate:predicate];
    int count=0;
    for (NSDictionary *  i in self.friendArray ){
        if ([[frx objectAtIndex:indexPath.row] isEqualToString:[i objectForKey:@"name"]]) {
           // [self toggleDelete:i];
            fr = [self.friendArray objectAtIndex:count];
            NSLog(@"%@",fr);
          //  indexF = count;
            
        }
        count++;
    }

}

@end
