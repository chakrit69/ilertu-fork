//
//  AddInViewController.h
//  iLertU
//
//  Created by Chakrit Paniam on 2/28/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTableViewCell.h"

@class UserEntity;
@class IndicatorView;

@interface AddInViewController : UITableViewController <UIActionSheetDelegate,UIAlertViewDelegate,NSFetchedResultsControllerDelegate> {
    IndicatorView * inDicView;

}
@property(nonatomic, retain)NSMutableArray *arrayOfData;
@property(nonatomic, retain)IBOutlet CustomTableViewCell *customCell;
@property(nonatomic, retain)UIToolbar* toolBar;

@property(nonatomic, retain)UserEntity *user;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;



-(id)initWithAddIn:(NSArray *)addin;

-(id)initWithUser:(UserEntity *)_user;

-(IBAction)buttonTap:(id)sender;

@end
