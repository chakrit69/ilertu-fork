//
//  UserEntity.m
//  iLertU
//
//  Created by Chakrit Paniam on 4/6/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "UserEntity.h"


@implementation UserEntity

@dynamic active;
@dynamic birthday;
@dynamic gender;
@dynamic idCard;
@dynamic idUser;
@dynamic imageUrl;
@dynamic lastedUpdateDate;
@dynamic name;
@dynamic password;
@dynamic phone;
@dynamic salutation;
@dynamic surname;
@dynamic username;

@end
