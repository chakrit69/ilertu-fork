//
//  SignInViewController.h
//  iLertU
//
//  Created by Chakrit Paniam on 3/10/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBConnect.h"
@class IndicatorView;

@interface SignInViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,FBRequestDelegate,FBDialogDelegate,FBSessionDelegate,UITextFieldDelegate,NSFetchedResultsControllerDelegate>{
    
    UITextField * userNameTF;
    UITextField * passWordTF;
    IndicatorView * inDicView;
    
}
@property (nonatomic, retain)IBOutlet UITableView *myTableView;
@property (nonatomic, retain)UIToolbar* toolBar;

@property (nonatomic, retain) NSArray *permissions;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

-(IBAction)signIn:(id)sender;

@end
