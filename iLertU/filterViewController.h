//
//  moniterViewController.h
//  lertViewController
//
//  Created by qoo hudsioo on 3/12/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AppDelegate;
@class UserEntity;
@interface filterViewController : UITableViewController{
    NSDictionary * fr;
    NSMutableArray * frIndex;
    NSMutableArray * frName;

    BOOL checkMark;
    NSInteger  indexCheckMark;
   
}
@property (nonatomic, retain) NSMutableArray * friendArray;
@property (nonatomic, retain) IBOutlet UIView * headerView;
@property (nonatomic, retain) AppDelegate * appDelegate;
//@property (nonatomic, retain) friendModel * frTrack;
@property (nonatomic, retain) IBOutlet UISegmentedControl * currentSeg;
@property (nonatomic, retain) UserEntity *user;
-(id)initWithUser:(UserEntity *)_user andFriendArray:(NSMutableArray*)fr_ary;
-(IBAction) switchControlIndexChanged;


@end
