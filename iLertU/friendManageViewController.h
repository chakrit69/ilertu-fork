//
//  friendManageViewController.h
//  iLertU
//
//  Created by qoo hudsioo on 4/1/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserEntity;

@interface friendManageViewController : UITableViewController<UIActionSheetDelegate,UIAlertViewDelegate>{
    NSDictionary * fr;
    NSInteger indexF;
    UIAlertView* alertViewDelete;
    UIAlertView* alertViewReject;
    
    
    NSMutableArray * frIndex;
    NSMutableArray * frName;
     UISwitch * tbSwitch;
    
    NSArray * showName;
    BOOL statusAll;
}

@property (nonatomic, retain) NSArray * showName;
@property (nonatomic, retain) IBOutlet UIView * headerView;
@property (nonatomic, retain) IBOutlet UISwitch *switchAll;
@property (nonatomic , retain) UserEntity *user;
@property (nonatomic, retain) NSMutableArray *friendArray;

-(id)initWithUser:(UserEntity *)_user;
- (IBAction)switchAllDidChange:(id)sender;
@end
