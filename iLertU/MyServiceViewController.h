//
//  MyServiceViewController.h
//  iLertU
//
//  Created by Chakrit Paniam on 3/11/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMLReader.h"

@class UserEntity;
@class Service;

@interface MyServiceViewController : UITableViewController <UIActionSheetDelegate,UIAlertViewDelegate>{
    XMLReader* xmlReader;
    int selectIndex;
}


@property (nonatomic, retain)UserEntity *user;
@property (nonatomic, retain)Service *service;
@property (nonatomic, retain)NSMutableArray *myServiceArray;


@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

-(id)initWithUser:(UserEntity *)_user;


@end
