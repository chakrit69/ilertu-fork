//
//  SignInViewController.m
//  iLertU
//
//  Created by Chakrit Paniam on 3/10/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "SignInViewController.h"
#import "WebViewController.h"
#import "AddInViewController.h"
#import "ASIFormDataRequest.h"
#import "UserEntity.h"
#import "SignUpViewController.h"

#import "XMLConverter.h"

@implementation SignInViewController


@synthesize myTableView;
@synthesize toolBar;

@synthesize permissions;

@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}




-(void)insertDataFormXML:(id)xmlResult{
   
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
          [inDicView stopAnimating];
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
        // NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
           
        UserEntity *user = (UserEntity *)[NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
            
        user.idUser = [[xmlResult objectForKey:@"user"] objectForKey:@"id"];
        user.name =   [[[xmlResult objectForKey:@"user"] objectForKey:@"name"] objectForKey:@"given"];
        user.salutation =[[xmlResult objectForKey:@"user"] objectForKey:@"salutation"];
        user.active = [[xmlResult objectForKey:@"user"] objectForKey:@"active"];
        user.surname = [[[xmlResult objectForKey:@"user"] objectForKey:@"name"] objectForKey:@"family"];
        user.idCard = [[xmlResult objectForKey:@"user"] objectForKey:@"idCard"];
        user.gender = [[xmlResult objectForKey:@"user"] objectForKey:@"gender"];
        user.birthday = [[xmlResult objectForKey:@"user"] objectForKey:@"birthday"];
        user.imageUrl = [[xmlResult objectForKey:@"user"] objectForKey:@"imageUrl"];
        user.phone = [[xmlResult objectForKey:@"user"] objectForKey:@"phone"];
        user.username = [[[xmlResult objectForKey:@"user"] objectForKey:@"authen"] objectForKey:@"username"];
        user.password = [[[xmlResult objectForKey:@"user"] objectForKey:@"authen"] objectForKey:@"password"];
        user.lastedUpdateDate =[xmlResult objectForKey:@"lastedUpdateDate"];
        
        AddInViewController *addInView = [[AddInViewController alloc] initWithUser:user];
        UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:addInView] autorelease];
        [self.navigationController presentModalViewController:navController animated:YES];
        [addInView release];
        
        // Save the context.
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    
        
    }
    
}

#pragma mark - Facebook API Calls

- (void)showLoggedIn {
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[delegate facebook] requestWithGraphPath:@"me" andDelegate:self];
    
}

- (void)fbDidLogin {
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[[delegate facebook] accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[[delegate facebook] expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    [self showLoggedIn];
    
}

/**
 * Make a Graph API Call to get information about the current logged in user.
 */


- (void)apiGraphUserPermissions {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[delegate facebook] requestWithGraphPath:@"me/permissions" andDelegate:self];
}


-(void)pressResetButton:(id)sender{
    WebViewController *webView = [[WebViewController alloc] initWithURL:@"http://www.sritown.com/manga/naruto/"];
    [self.navigationController pushViewController:webView animated:YES];
    [webView release];
    
}
-(void)pressJoinButton:(id)sender{
    SignUpViewController *signup  = [[SignUpViewController alloc] initWithNibName:@"SignUpViewController" bundle:nil];
    [self.navigationController pushViewController:signup animated:YES];
    [signup release];
    /*
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![[delegate facebook] isSessionValid]) {
        [[delegate facebook] authorize:permissions];
    } else {
        [self showLoggedIn];
    }
     */
    
}
-(void)setupUI{
    
    self.title = @"Sign In";
    self.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"bg_iphone.png"]];
    
    self.toolBar = [UIToolbar new];
    self.toolBar.barStyle = UIBarStyleBlack;
    [self.toolBar sizeToFit];
    self.toolBar.frame = CGRectMake(0, 430, 320, 50);
    
    //Add buttons
    UIBarButtonItem *systemItem1 = [[UIBarButtonItem alloc]initWithTitle:@"Reset Password" style:UIBarButtonItemStyleBordered target:self action:@selector(pressResetButton:)];
    
    UIBarButtonItem *systemItem2 = [[UIBarButtonItem alloc]initWithTitle:@"Join Now" style:UIBarButtonItemStyleBordered 
                                                                                 target:self
                                                                                 action:@selector(pressJoinButton:)];
    
    
    
    //Use this to put space in between your toolbox buttons
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    
    //Add buttons to the array
    NSArray *items = [NSArray arrayWithObjects: systemItem1, flexItem, flexItem, flexItem, systemItem2, nil];
    
    //release buttons
    [systemItem1 release];
    [systemItem2 release];
    [flexItem release];
    
    //add array of buttons to toolbar
    
    
    [self.toolBar setItems:items animated:NO];
    [self.navigationController.view addSubview:self.toolBar];

}

- (IBAction)signIn:(id)sender{
    
    [inDicView startAnimating];
    self.navigationItem.hidesBackButton = YES;
    if (userNameTF.text != NULL && passWordTF.text != NULL) {
        
        NSString *pathUrl = [NSString stringWithFormat:@"%@/user/sign-in",ILERTU];
        NSURL *url = [NSURL URLWithString:pathUrl];
        
        
        ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
        
        [request2 addRequestHeader:@"content-type" value:@"text/xml"];
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
        NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
        
        NSString *Language;
        Language = [plistDict objectForKey:@"Language"];
        
        
        NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>                            <lertParams type='ilertu.user.sign-in'>                            <common>                            <device>                            <deviceType>ios</deviceType>                            <deviceId>%@</deviceId>                            <currentVersion>1.0a</currentVersion>                            </device>                            <authen>                            <username>%@</username>                            <password>%@</password>                            </authen>                            <lastedUpdateDate>20120131235959999</lastedUpdateDate>                            <language>%@</language>                            </common>                            </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],userNameTF.text,passWordTF.text,Language];
        
        
        
        [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [request2 setCompletionBlock:^{
            NSString *responseString = [request2 responseString];
            
            XMLConverter *xmlConverter = [[XMLConverter alloc] init];
           [self insertDataFormXML:[xmlConverter parseWithString:responseString]];
            
            
            
            
        }];
        
        [request2 setFailedBlock:^{
            
            NSError *error = [request2 error];
            NSLog(@"Error: %@", error.localizedDescription);
            
            UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [inDicView stopAnimating];
            [alertMessageView show];
            [alertMessageView release];
            
        }];
        
        [request2 startAsynchronous];
    }
    else {
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:@"Please insert information" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    
    
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUI];
    
    if (__managedObjectContext == nil) 
    { 
        __managedObjectContext = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext]; 
    }
    
    permissions = [[NSArray alloc] initWithObjects:@"offline_access", nil];
    [self.myTableView setBackgroundColor:[UIColor clearColor]];
    // Do any additional setup after loading the view from its nib.
    
    userNameTF = [[UITextField alloc] initWithFrame:CGRectMake(15, 10, 180, 30)];
    userNameTF.adjustsFontSizeToFitWidth = YES;
    userNameTF.textColor = [UIColor blackColor];
    
    passWordTF = [[UITextField alloc] initWithFrame:CGRectMake(15, 10, 180, 30)];
    passWordTF.adjustsFontSizeToFitWidth = YES;
    passWordTF.textColor = [UIColor blackColor];
    
    userNameTF.delegate = self;
    passWordTF.delegate = self;
 
    inDicView = [[IndicatorView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];  
    [self.view addSubview:inDicView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self.toolBar removeFromSuperview];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
        
    // Configure the cell...
    
    if (indexPath.row == 0) {
    
        userNameTF.placeholder = @"Username";
        userNameTF.keyboardType = UIKeyboardTypeEmailAddress;
        userNameTF.returnKeyType = UIReturnKeyNext;
        [cell addSubview:userNameTF];
    
        
    }else if(indexPath.row == 1) {
        passWordTF.placeholder = @"Password";
        passWordTF.keyboardType = UIKeyboardTypeDefault;
        passWordTF.returnKeyType = UIReturnKeyDefault;
        passWordTF.secureTextEntry = YES;
        [cell addSubview:passWordTF];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (void)moveView:(UIView *)view duration:(NSTimeInterval)duration
            curve:(int)curve x:(CGFloat)x y:(CGFloat)y
{
    // Setup the animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    // The transform matrix
    CGAffineTransform transform = CGAffineTransformMakeTranslation(x, y);
    view.transform = transform;
    
    // Commit the changes
    [UIView commitAnimations];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self moveView:self.view duration:0.1 curve:UIViewAnimationCurveLinear x:0 y:-40];
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if(theTextField==userNameTF){
        [passWordTF becomeFirstResponder];

    }else if(theTextField == passWordTF){
        [passWordTF resignFirstResponder];
         [self moveView:self.view duration:0.1 curve:UIViewAnimationCurveLinear x:0 y:0];
    }
    return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - FBRequestDelegate Methods
/**
 * Called when the Facebook API request has returned a response. This callback
 * gives you access to the raw response. It's called before
 * (void)request:(FBRequest *)request didLoad:(id)result,
 * which is passed the parsed response object.
 */
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    //NSLog(@"received response");
}

/**
 * Called when a request returns and its response has been parsed into
 * an object. The resulting object may be a dictionary, an array, a string,
 * or a number, depending on the format of the API response. If you need access
 * to the raw response, use:
 *
 * (void)request:(FBRequest *)request
 *      didReceiveResponse:(NSURLResponse *)response
 */

- (void)request:(FBRequest*)request didLoadRawResponse:(NSData*)data
{
    NSString *response = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSLog(@"%@", response);
    [response release];
}

- (void)request:(FBRequest *)request didLoad:(id)result {
    if ([result isKindOfClass:[NSDictionary class]])
    {
        NSString *email = [result objectForKey: @"email"];
        NSString *name = [result objectForKey: @"name"];
        NSLog(@"Name = %@",name);
        NSLog(@"E-mail = %@",email);
        
       
        NSMutableArray *arrayOfSampleData=[NSMutableArray arrayWithObjects:@"Arunsawad",@"Doo D",@"Enmergency Call",@"iService",@"Lert",nil];
        AddInViewController *addInViewController = [[AddInViewController alloc] initWithAddIn:arrayOfSampleData];
        
        
        UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:addInViewController] autorelease];
        [self presentModalViewController:navController animated:YES];
        [addInViewController release];
    }
    
}

/**
 * Called when an error prevents the Facebook API request from completing
 * successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"Err message: %@", [[error userInfo] objectForKey:@"error_msg"]);
    NSLog(@"Err code: %d", [error code]);
}



#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (__fetchedResultsController != nil) {
        return __fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserEntity" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"name" ascending:NO] autorelease];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"] autorelease];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return __fetchedResultsController;
}   




@end
