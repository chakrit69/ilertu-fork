//
//  XMLReader.m
//  TESTTouchXML
//
//  Created by Chakrit Paniam on 2/27/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "XMLReader.h"
#import "CXMLDocument.h"
#import "CXMLElement.h"

@implementation XMLReader

@synthesize urlName;
@synthesize nodeName;
@synthesize result;
@synthesize XMLString;

-(id)initWithURL:(NSString *)url andNode:(NSString *)node{
    [super init];
    self.urlName = url;
    self.nodeName = node;
    return self;
}
+(id)xmlWithURL:(NSString *)url andNode:(NSString *)node{
    return [[[XMLReader alloc] initWithURL:url andNode:node] autorelease];
}


-(id)initWithXMLString:(NSString *)xml andNode:(NSString *)node{
    [super init];
    self.XMLString = xml;
    self.nodeName = node;
    return self;
}
+(id)xmlWithXMLString:(NSString *)xml andNode:(NSString *)node{
    return [[[XMLReader alloc] initWithXMLString:xml andNode:node] autorelease];
}

-(void)parseXML{
    
    BOOL haveName = NO;
    NSMutableArray *res = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
    
    CXMLDocument *doc = [[[CXMLDocument alloc] initWithXMLString:self.XMLString options:0 error:nil] autorelease];
    NSArray *nodes = NULL;
    
    nodes = [doc nodesForXPath:self.nodeName error:nil];
    for (CXMLElement *node in nodes) {
        
        NSMutableArray *arrayNode = [[NSMutableArray alloc] init];
        int counter;
        for(counter = 0; counter < [node childCount]; counter++) {
            NSLog(@"[node childCount] = %d",counter);
            
            CXMLElement *childNode = [[node children] objectAtIndex:counter];     
           
            

            if ([[childNode name] isEqualToString:@"addIn"]) {
                NSMutableDictionary *subitem = [[NSMutableDictionary alloc] init];
                haveName = YES;
                NSLog(@"Add IN !!");
                if ([[childNode attributeForName:@"id"] stringValue]) {
                    NSLog(@"add %@",[[childNode attributeForName:@"id"] stringValue]);
                    [subitem setObject:[[childNode attributeForName:@"id"] stringValue] forKey:@"id"];
                    NSLog(@"SET addin ID law");
                    
                }
                [arrayNode addObject:subitem];
                NSArray *subChildNode = [childNode children];
                for (int i = 0; i< [subChildNode count]; i++) {
                    CXMLElement *resultElement = [subChildNode objectAtIndex:i];
                    [subitem setObject:[resultElement stringValue] forKey:[resultElement name]];
                }
    
               // [item setObject:[[childNode childAtIndex:counter] stringValue] forKey:[[node childAtIndex:counter] name]];
                
                
            }
            else {
                [item setObject:[[node childAtIndex:counter] stringValue] forKey:[[node childAtIndex:counter] name]];
            }
        }
        [item setObject:arrayNode forKey:@"addins"];
        
        CXMLElement *childNode = [[node children] objectAtIndex:0];
        
        if ([childNode nodesForXPath:@"//authen" error:nil]) {
            NSArray *subnode = nil;
            subnode = [childNode nodesForXPath:@"//authen" error:nil];
            for (CXMLElement *resultInSub in subnode) {
                for(counter = 0; counter <= [childNode childCount]; counter++) {

                    [item setObject:[[resultInSub childAtIndex:counter] stringValue] forKey:[[resultInSub childAtIndex:counter] name]];
                }            
            }
        }
        if ([childNode nodesForXPath:@"//name" error:nil]) {
            NSArray *subnode = nil;
            subnode = [childNode nodesForXPath:@"//name" error:nil];
            
            for (CXMLElement *resultInSub in subnode) {
                for(counter = 0; counter <= [childNode childCount]; counter++) {
                  //  NSLog(@"subnode name = %@",[[resultInSub childAtIndex:counter] name]);
                    if (haveName == NO) {
                       [item setObject:[[resultInSub childAtIndex:counter] stringValue] forKey:[[resultInSub childAtIndex:counter] name]];
                    }
                }            
            }
        }
        
        if ([childNode nodesForXPath:@"//social/facebook" error:nil]) {
            
        }
        if ([childNode nodesForXPath:@"//social/twitter" error:nil]) {
            
        }
        
        if ([[node attributeForName:@"id"] stringValue]) {
            NSLog(@"user id == %@",[node stringValue]);
            [item setObject:[[node attributeForName:@"id"] stringValue] forKey:@"id"];  
            
        }
        
          
        [res addObject:item];
        [item release];
    }
    nodes = [doc nodesForXPath:@"//lastedUpdateDate" error:nil];
    
    for (CXMLElement *node in nodes) {
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        NSLog(@"node lastupdate");
        [item setObject:[node stringValue] forKey:[node name]];
        [res addObject:item];
    }
    
    
    if ([res count] == 0) {
        
        self.nodeName = @"//error";
        
        CXMLDocument *doc = [[[CXMLDocument alloc] initWithXMLString:self.XMLString options:0 error:nil] autorelease];
        NSArray *nodes = NULL;
        //  searching for piglet nodes
        nodes = [doc nodesForXPath:self.nodeName error:nil];
        for (CXMLElement *node in nodes) {
            
            
            int counter;
            
            for(counter = 0; counter < [node childCount]; counter++) {
                //  common procedure: dictionary with keys/values from XML node
                
                [item setObject:[[node childAtIndex:counter] stringValue] forKey:[[node childAtIndex:counter] name]];
            }
            
            [res addObject:item];
            [item release];
            
        }
    }
    
    //  and we print our results
    result = [NSArray arrayWithArray:res];
    [res release];
    
}


@end
