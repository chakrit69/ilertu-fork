//
//  WebViewController.h
//  iLertU
//
//  Created by Chakrit Paniam on 3/9/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IndicatorView;
@interface WebViewController : UIViewController<UIWebViewDelegate>{
    IndicatorView * inDicView;
}
@property (nonatomic, retain) IBOutlet UIWebView* webView;
@property (nonatomic, retain) NSString* webURL;

-(id)initWithURL:(NSString *)url;

@end
