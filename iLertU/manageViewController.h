//
//  manageViewController.h
//  lertViewController
//
//  Created by qoo hudsioo on 3/11/55 BE.
//  Copyright (c) 2555 QOOHOUSE. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDelegate;
@class UserEntity;
@interface manageViewController : UITableViewController{
    
    NSString * pendingNumber;
    
}

@property (nonatomic, retain) AppDelegate * appDelegate;
@property (nonatomic, retain) UserEntity *user;

@property (nonatomic, retain) NSArray *requestArray;

-(id)initWithUser:(UserEntity *)_user;

@end
