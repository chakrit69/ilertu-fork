//
//  AddInViewController.m
//  iLertU
//
//  Created by Chakrit Paniam on 2/28/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "AddInViewController.h"

#import "LogInViewController.h"

#import "SettingViewController.h"

#import "WebViewController.h"

#import "EnmergencyCallViewController.h"

#import "MyServiceViewController.h"

#import "UserEntity.h"

#import "ASIFormDataRequest.h"

#import "AppDelegate.h"

#import "NSDataAdditions.h"

#import "EditProfileViewController.h"
#import "IndicatorView.h"

#import "locationViewController.h"
#import "manageViewController.h"
#import "lertsViewController.h"




#import "XMLConverter.h"

@implementation AddInViewController

@synthesize arrayOfData;
@synthesize customCell;
@synthesize toolBar;
@synthesize user;


@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;

-(void)insertAddFormXML:(id)xmlResult{
    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        [inDicView stopAnimating];
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        //NSLog(@"RESULT = %@",[[[xmlResult objectForKey:@"addins"] objectAtIndex:1] class]);
        //NSLog(@"%d",[self.arrayOfData count]);
        for (int i = 0 ; i< [[xmlResult objectForKey:@"addins"]count]; i++) {
            [self.arrayOfData addObject:[[xmlResult objectForKey:@"addins"] objectAtIndex:i]];
        }
        
    }
    [self.tableView reloadData];
    
}

-(void)getInformation{
    
    NSString *pathUrl = [NSString stringWithFormat:@"%@/apps/add-ins/list",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?><lertParams type='ilertu.add-ins-list'><common><device><deviceType>ios</deviceType><deviceId>%@</deviceId><currentVersion>1.0a</currentVersion></device><authen><username>%@</username><password>%@</password></authen><lastedUpdateDate>0</lastedUpdateDate><language>%@</language></common></lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.username,self.user.password,Language];
    
    
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
                  
        XMLConverter *xml = [[XMLConverter alloc] init];
        

        
        //ใส่ข้อมูลลง array;
        
        [self insertAddFormXML:[xml parseWithString:responseString]];
        
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    


}

-(void)insertDataFormXML:(id)xmlResult{
    
    if ([xmlResult isKindOfClass:[NSArray class]] && [[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"]){
        //NSLog(@"ERROR");
        [inDicView stopAnimating];
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"Error" message:[[xmlResult objectAtIndex:0] objectForKey:@"errorDesc"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
    }
    else{
        
        
       
        self.user.idUser = [[xmlResult objectForKey:@"user"] objectForKey:@"id"];
        self.user.name =   [[[xmlResult objectForKey:@"user"] objectForKey:@"name"] objectForKey:@"given"];
        self.user.salutation =[[xmlResult objectForKey:@"user"] objectForKey:@"salutation"];
        self.user.active = [[xmlResult objectForKey:@"user"] objectForKey:@"active"];
        self.user.surname = [[[xmlResult objectForKey:@"user"] objectForKey:@"name"] objectForKey:@"family"];
        self.user.idCard = [[xmlResult objectForKey:@"user"] objectForKey:@"idCard"];
        self.user.gender = [[xmlResult objectForKey:@"user"] objectForKey:@"gender"];
        self.user.birthday = [[xmlResult objectForKey:@"user"] objectForKey:@"birthday"];
        self.user.imageUrl = [[xmlResult objectForKey:@"user"] objectForKey:@"imageUrl"];
        self.user.phone = [[xmlResult objectForKey:@"user"] objectForKey:@"phone"];
        self.user.username = [[[xmlResult objectForKey:@"user"] objectForKey:@"authen"] objectForKey:@"username"];
        self.user.password = [[[xmlResult objectForKey:@"user"] objectForKey:@"authen"] objectForKey:@"password"];
        self.user.lastedUpdateDate =[xmlResult objectForKey:@"lastedUpdateDate"];
        
        NSLog(@"User id : %@",self.user.idUser);
        
        
    }
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithAddIn:(NSArray *)addin{
    self = [super initWithNibName:@"AddInViewController" bundle:nil];
    if (self) {
        self.arrayOfData = [NSArray arrayWithArray:addin];
    }
    return self;
}

-(id)initWithUser:(UserEntity *)_user{
    self = [super initWithNibName:@"AddInViewController" bundle:nil];
    if (self) {
        self.user = _user;
        
        NSDictionary *addin1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"Arunsawad",@"desc",
                                @"001",@"id",
                                @"Arunsawad",@"imageUrl",
                                @"Arunsawad",@"name",
                                @"http://www.arunsawad.com",@"webUrl",
                                @"true",@"isNew",
                                @"true",@"showWebpage",nil];
        NSDictionary *addin2 = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"Doo D",@"desc",
                                @"002",@"id",
                                @"Doo D",@"imageUrl",
                                @"Doo D",@"name",
                                @"http://www.doo-d.com",@"webUrl",
                                @"true",@"isNew",
                                @"true",@"showWebpage",nil];
        NSDictionary *addin3 = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"Enmergency Call",@"desc",
                                @"003",@"id",
                                @"Enmergency Call",@"imageUrl",
                                @"Enmergency Call",@"name",
                                @"http://www.gogle.com",@"webUrl",
                                @"true",@"isNew",
                                @"true",@"showWebpage",nil];
        NSDictionary *addin4 = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"iService",@"desc",
                                @"004",@"id",
                                @"iService",@"imageUrl",
                                @"iService",@"name",
                                @"http://www.google.com",@"webUrl",
                                @"true",@"isNew",
                                @"true",@"showWebpage",nil];
        
        NSDictionary *addin5 = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"Lert",@"desc",
                                @"005",@"id",
                                @"Lert",@"imageUrl",
                                @"Lert",@"name",
                                @"http://www.arunsawad.com",@"webUrl",
                                @"true",@"isNew",
                                @"true",@"showWebpage",nil];

        self.arrayOfData = [[NSMutableArray alloc]init];
        [self.arrayOfData addObject:addin1];
        [self.arrayOfData addObject:addin2];
        [self.arrayOfData addObject:addin3];
        [self.arrayOfData addObject:addin4];
        [self.arrayOfData addObject:addin5];
                

        [self getInformation];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)pressAccountButton:(id)sender{
    
    UIActionSheet *actionAccount = [[UIActionSheet alloc] initWithTitle:@"Action for" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Sign Out" otherButtonTitles:@"Edit Profile", @"Change Password", nil];
    actionAccount.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionAccount showInView:self.view];

    [actionAccount release];

    
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) { 
        //Sign Out
        
        
        NSError ** error;
        // retrieve the store URL
        NSURL * storeURL = [[__managedObjectContext persistentStoreCoordinator] URLForPersistentStore:[[[__managedObjectContext persistentStoreCoordinator] persistentStores] lastObject]];
        // lock the current context
        [__managedObjectContext lock];
        [__managedObjectContext reset];//to drop pending changes
        //delete the store from the current managedObjectContext
        if ([[__managedObjectContext persistentStoreCoordinator] removePersistentStore:[[[__managedObjectContext persistentStoreCoordinator] persistentStores] lastObject] error:error])
        {
            // remove the file containing the data
            [[NSFileManager defaultManager] removeItemAtURL:storeURL error:error];
            //recreate the store like in the  appDelegate method
            [[__managedObjectContext persistentStoreCoordinator] addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:error];//recreates the persistent store
        }
        [__managedObjectContext unlock];        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sign Out" message:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        
    } else if (buttonIndex == 1) {
        
        
        self.toolBar.hidden = YES;

        if (self.user == nil) {
            
        }
        else {
            EditProfileViewController *editProfile = [[EditProfileViewController alloc] initWithUser:self.user];
            [self.navigationController pushViewController:editProfile animated:YES];

        }
        //Edit Profile
       /* if (user == nil) {
            else {
            EditProfileViewController *editProfile = [[EditProfileViewController alloc] initWithUser:user];
            [self.navigationController pushViewController:editProfile animated:YES];
        }
        */

    } else if (buttonIndex == 2) {

        //Change Password
        WebViewController *webView = [[[WebViewController alloc] initWithURL:@"http://arunsawad.com/"] autorelease];
        self.toolBar.hidden = YES;
        [self.navigationController pushViewController:webView animated:YES];

    } else if (buttonIndex == 3) {

        //Cancle
        

    }

}
- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // the user clicked one of the OK/Cancel buttons
    if (buttonIndex == 0)
    {
        //NSLog(@"cancel");
    }
    else
    {   
        //NSLog(@"ok");
        LogInViewController *loginView = [[LogInViewController alloc] initWithNibName:@"LogInViewController" bundle:nil];
        UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:loginView] autorelease];
        [self.navigationController presentModalViewController:navController animated:YES];
        [loginView release];
    }
}

-(void)pressSettingButton:(id)sender{
    self.toolBar.hidden = YES;
    SettingViewController *settingView = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
   
  //  UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:settingView] autorelease];
    [self.navigationController pushViewController:settingView animated:YES];
    [settingView release];
    

}

-(NSMutableArray *)setUpData
{
    NSMutableArray *arrayOfSampleData=[NSMutableArray arrayWithObjects:@"addin_arun",@"addin_dod",@"addin_call",@"addin_lert",nil];
    return arrayOfSampleData;
}
-(void)setupUI{
    
    //self.arrayOfData=[self setUpData];
	self.tableView.separatorColor=[UIColor clearColor];
	self.navigationController.navigationBar.tintColor=[UIColor colorWithRed:0.3 green:0.7 blue:0.7 alpha:1.0];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    self.parentViewController.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"addin_bg.png"]];
    
    self.toolBar = [UIToolbar new];
    self.toolBar.barStyle = UIBarStyleBlack;
    [self.toolBar sizeToFit];
    self.toolBar.frame = CGRectMake(0, 430, 320, 50);
    
    //Add buttons
    UIBarButtonItem *systemItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                                 target:self
                                                                                 action:@selector(pressAccountButton:)];
    
    UIBarButtonItem *systemItem2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose
                                                                                 target:self
                                                                                 action:@selector(pressSettingButton:)];
    

    
    //Use this to put space in between your toolbox buttons
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    
    //Add buttons to the array
    NSArray *items = [NSArray arrayWithObjects: systemItem1, flexItem, flexItem, flexItem, systemItem2, nil];
    
    //release buttons
    [systemItem1 release];
    [systemItem2 release];
    [flexItem release];
    
    //add array of buttons to toolbar
    

    [self.toolBar setItems:items animated:NO];
    [self.navigationController.view addSubview:self.toolBar];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"iLertU";
    [self setupUI];
    
    
    
    if (__managedObjectContext == nil) 
    { 
        __managedObjectContext = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext]; 
    }

    
    inDicView = [[IndicatorView alloc]initWithFrame:CGRectMake(0, 0, 50, 50) ];
 

}
-(void)getUserInformation{
    NSString *pathUrl = [NSString stringWithFormat:@"%@/user/get",ILERTU];
    NSURL *url = [NSURL URLWithString:pathUrl];
    
    
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url];
    
    [request2 addRequestHeader:@"content-type" value:@"text/xml"];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent: @"Setting.plist"];
    NSMutableDictionary* plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *Language;
    Language = [plistDict objectForKey:@"Language"];
    
    
    NSString *string = [NSString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?><lertParams type='ilertu.user.get'>                        <common>                        <device>                        <deviceType>ios</deviceType>                        <deviceId>%@</deviceId>                        <currentVersion>1.0a</currentVersion>                        </device>                        <authen/>                        <lastedUpdateDate>%@</lastedUpdateDate>                        <language>%@</language>                        </common>                        <user id='%@'/>                        </lertParams>",[[UIDevice currentDevice] uniqueIdentifier],self.user.lastedUpdateDate,Language,self.user.idUser];
    
    
    
    [request2 setPostBody:(NSMutableData*)[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request2 setCompletionBlock:^{
        NSString *responseString = [request2 responseString];
        
        XMLConverter *xmlConverter = [[[XMLConverter alloc] init] autorelease];
     
            
       // xmlReader.XMLString = [request2 responseString];
        
        
      //  NSLog(@"get user%@",xmlReader.result);
            
                
         [self insertDataFormXML:[xmlConverter parseWithString:responseString]];
        
        
    }];
    
    [request2 setFailedBlock:^{
        NSError *error = [request2 error];
        NSLog(@"Error: %@", error.localizedDescription);
        
        UIAlertView *alertMessageView = [[UIAlertView alloc] initWithTitle:@"I Lert U" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertMessageView show];
        [alertMessageView release];
        
    }];
    
    [request2 startAsynchronous];
    
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getUserInformation];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.toolBar.hidden = NO;

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{


        int count=self.arrayOfData.count%3;
        if(count==0)
        {
            return self.arrayOfData.count/3;
        }
        else
        {
            return (self.arrayOfData.count/3) +1;
        }
        

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        static NSString *CellIdentifier = @"Cell1";
        
        CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"CustomTableViewCell" owner:self options:nil];
            cell = customCell;
        }
        int arrayCount=self.arrayOfData.count;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        cell.backgroundColor=[UIColor clearColor];  
        
        NSString *buttonText=[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+0] objectForKey:@"desc"];
        
        NSURL* aURL = [NSURL URLWithString:[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+0] objectForKey:@"imageUrl"]];
        
        NSData* data = [[NSData alloc] initWithContentsOfURL:aURL];
        if (data == nil) {
            NSString *filePath = [[NSBundle mainBundle] pathForResource:[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+0] objectForKey:@"imageUrl"] ofType:@"png"];  
            NSData *data2 = [NSData dataWithContentsOfFile:filePath];
            data = data2;
        
        }
        
        [cell.leftSideButton setBackgroundImage:[UIImage imageWithData:data] forState:UIControlStateNormal] ;
        [cell.leftSideButton setContentMode:UIViewContentModeCenter] ;
        cell.leftSideButton.backgroundColor=[UIColor clearColor]; 
        cell.leftSideButton.tag = ((indexPath.row)*3)+0;
        cell.leftSideLabel.text=buttonText;
        
        if(((indexPath.row)*3)+2<arrayCount)
        {
            
            buttonText=[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+1] objectForKey:@"desc"];
            NSURL* aURL = [NSURL URLWithString:[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+1] objectForKey:@"imageUrl"]];
            NSData* data = [[NSData alloc] initWithContentsOfURL:aURL];
            
            if (data == nil) {
                NSString *filePath = [[NSBundle mainBundle] pathForResource:[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+1] objectForKey:@"imageUrl"] ofType:@"png"];  
                NSData *data2 = [NSData dataWithContentsOfFile:filePath];
                data = data2;
                
            }

            
            [cell.middleSideButton setBackgroundImage:[UIImage imageWithData:data] forState:UIControlStateNormal] ;
            [cell.middleSideButton setContentMode:UIViewContentModeCenter] ;
            cell.middleSideButton.backgroundColor=[UIColor clearColor]; 
            
            cell.middleSideButton.tag = ((indexPath.row)*3)+1 ;
            
            cell.middleSideLabel.text=buttonText;
            
            buttonText=[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+2] objectForKey:@"desc"];
            aURL = [NSURL URLWithString:[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+2] objectForKey:@"imageUrl"]];
            data = [[NSData alloc] initWithContentsOfURL:aURL];
            
            if (data == nil) {
                NSString *filePath = [[NSBundle mainBundle] pathForResource:[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+2] objectForKey:@"imageUrl"] ofType:@"png"];  
                NSData *data2 = [NSData dataWithContentsOfFile:filePath];
                data = data2;
                
            }

            [cell.rightSideButton setBackgroundImage:[UIImage imageWithData:data] forState:UIControlStateNormal] ;
            [cell.rightSideButton setContentMode:UIViewContentModeCenter] ;
            cell.rightSideButton.backgroundColor=[UIColor clearColor]; 
            
            cell.rightSideButton.tag = ((indexPath.row)*3)+2 ;
          //  cell.rightSideLabel.textColor = [UIColor colorWithRed:0.16 green:0.47 blue:0.02 alpha:1.0] ;
            
            cell.rightSideLabel.text=buttonText;
            
            
        }
        else if(((indexPath.row)*3)+1<arrayCount)
        {
            
            buttonText=[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+1] objectForKey:@"desc"];
            NSURL* aURL = [NSURL URLWithString:[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+1] objectForKey:@"imageUrl"]];
            NSData* data = [[NSData alloc] initWithContentsOfURL:aURL];
            
            
            if (data == nil) {
                NSString *filePath = [[NSBundle mainBundle] pathForResource:[[self.arrayOfData objectAtIndex:((indexPath.row)*3)+1] objectForKey:@"imageUrl"] ofType:@"png"];  
                NSData *data2 = [NSData dataWithContentsOfFile:filePath];
                data = data2;
                
            }

            
            [cell.middleSideButton setBackgroundImage:[UIImage imageWithData:data] forState:UIControlStateNormal] ;
            [cell.middleSideButton setContentMode:UIViewContentModeCenter] ;
            cell.middleSideButton.backgroundColor=[UIColor clearColor]; 
            
            cell.middleSideButton.tag = ((indexPath.row)*3)+1 ;
            
            cell.middleSideLabel.text=buttonText;
            
            cell.rightSideButton.hidden=YES;
            cell.rightSideLabel.hidden=YES;
            
        }
        else
        {
            cell.middleSideButton.tag = 10 ;
            cell.middleSideLabel.hidden=YES;
            cell.middleSideButton.hidden=YES;
            
            cell.rightSideButton.hidden=YES;
            cell.rightSideLabel.hidden=YES;
            
        }
        
        
        
        return cell;
 //   }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {  
	
    if (self.tableView.editing) {
        return 40;
    }
    else
    {
        return 100 ;
    }
    //	return heightOfEachRow ;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(IBAction)buttonTap:(id)sender{
    if ([sender tag] == 0) {
        WebViewController *webView = [[[WebViewController alloc] initWithURL:@"http://arunsawad.com/"] autorelease];
        self.toolBar.hidden = YES;
        [self.navigationController pushViewController:webView animated:YES];
    }
    if ([sender tag] == 1) {
        WebViewController *webView = [[[WebViewController alloc] initWithURL:@"http://www.youtube.com/"] autorelease];
        self.toolBar.hidden = YES;
        [self.navigationController pushViewController:webView animated:YES];
        
    }
    if ([sender tag] == 2) {
        EnmergencyCallViewController *callView = [[EnmergencyCallViewController alloc] initWithNibName:@"EnmergencyCallViewController" bundle:nil];
        self.toolBar.hidden = YES;
        [self.navigationController pushViewController:callView animated:YES];
       

    }
    if ([sender tag] == 3) {
        self.toolBar.hidden = YES;
        MyServiceViewController *myService = [[MyServiceViewController alloc] initWithUser:self.user];
        [self.navigationController pushViewController:myService animated:YES];
        [myService release];
        
    }
    if ([sender tag] == 4) {
        self.toolBar.hidden = YES;
        locationViewController *locationVC = [[locationViewController alloc] initWithUser:self.user];
        manageViewController *manageVC = [[manageViewController alloc] initWithUser:self.user];
        lertsViewController *lertsVC = [[lertsViewController alloc] initWithNibName:@"lertsViewController" bundle:nil];
      
        UINavigationController * naviLocation = [[[UINavigationController alloc]initWithRootViewController:locationVC]autorelease];
        UINavigationController * naviManage = [[[UINavigationController alloc]initWithRootViewController:manageVC]autorelease];
        UINavigationController * naviLert = [[[UINavigationController alloc]initWithRootViewController:lertsVC]autorelease];
        
        UITabBarController * tabBarController = [[[UITabBarController alloc] init] autorelease];
        
        tabBarController.viewControllers = [NSArray arrayWithObjects:naviLocation,naviLert,naviManage, nil];
        
        [self.navigationController presentModalViewController:tabBarController animated:YES];
    }
    
    else if ([sender tag] >=5) {
        NSString *string = [[self.arrayOfData objectAtIndex:[sender tag]] objectForKey:@"webUrl"];
        WebViewController *webView = [[WebViewController alloc] initWithURL:string];
        self.toolBar.hidden = YES;
        [self.navigationController pushViewController:webView animated:YES];
    }
    

}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (__fetchedResultsController != nil) {
        return __fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserEntity" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"name" ascending:NO] autorelease];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"] autorelease];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return __fetchedResultsController;
}   


@end
